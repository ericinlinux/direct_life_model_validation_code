"""
contagion_model_pure.py
Created by: Eric Araujo

+++++ This file has the contagion model and this model can be run for the three modes:
        (1) Original
        (2) Weighted
        (3) Logistic
This model uses only the initial PAL as a way to predict the next steps.

This function also considers that all the agents have the same value for expressiveness and openessess.

Last change: 2016-10-25
"""

import numpy as np
import pickle
import pandas as pd



"""
Model = "original" -> old
Model = "weighted" -> new with weighted
Model = "logistic" -> new with logistic
"""
def run_contagion_mode(graphs, model = 'original'):
    steps = len(graphs)
    delta = 1.0
    openn = 0.5
    exp = 0.5

    count = 0
    isolatedNodes = []

    for node in graphs[0].nodes():
        graphs[0].node[node]['pal'] = {0: graphs[0].node[node]['initial_goal']}

    for t in np.arange(1, len(graphs)):
        g_pre = graphs[t - 1]
        g_t = graphs[t]
        # print t

        # List of previous nodes
        nodes_pre = list(graphs[t - 1].nodes())

        for node in graphs[t].nodes():
            # If it is an existing node, copy it
            if node in nodes_pre:
                graphs[t].node[node]['pal'] = graphs[t - 1].node[node]['pal']
            # If it is a new node, initialize it!
            else:
                graphs[t].node[node]['pal'] = {t: graphs[t].node[node]['initial_goal']}
        # Counting isolated nodes
        isolatedNodes.append((t, count))
        count = 0

        for node in graphs[t].nodes():
            # If it is a new node, skip
            if node not in nodes_pre:
                continue
            # coefficient to calculate the aggimpact
            cs = 0.0
            aggimpact = 0.0
            speed_factor = 0.0
            # Take the neighbors for the time t-1 (new nodes does influence for next step)
            num_neighbours = len(graphs[t - 1].neighbors(node))

            if num_neighbours == 0:
                count += 1
            # Calculate speed factor based on time t-1
            # The values for exp and openn in comments are the way to use a different value for each agent
            for neigh in graphs[t - 1].neighbors(node):
                # exp = graph.node[neigh]['expressivenness']
                connect = 1  # g.get_edge_data(neigh,node).values()[0]
                # openn = graph.node[node]['openness']
                wba = exp * connect * openn
                speed_factor = speed_factor + wba
                cs = cs + exp * connect

            # Calculate the qStart (or aggimpact)
            for neigh in graphs[t - 1].neighbors(node):
                # exp = graph.node[neigh]['expressivenness']
                connect = 1  # graph.get_edge_data(neigh, node).values()[0]
                try:
                    # It has a copy of the previous date - taking the second argument of the tuple ([1])
                    v_neigh = graphs[t - 1].node[neigh]['pal'][t - 1]
                except:
                    print 'Node ' + str(neigh) + '\n'
                    raw_input()
                if cs == 0:
                    aggimpact = 0
                else:
                    aggimpact = aggimpact + (exp * connect * v_neigh) / cs
            try:
                old_pal = graphs[t - 1].node[node]['pal'][t - 1]
            except:
                print node, graphs[t - 1].node[node]['pal']
                raw_input()
            # Definition of the speed factor
            if model == 'original':
                new_pal = old_pal + speed_factor * (aggimpact - old_pal) * delta
            elif model == 'weighted':
                if num_neighbours == 0:
                    new_pal = old_pal
                else:
                    new_pal = old_pal + (speed_factor / num_neighbours) * (aggimpact - old_pal) * delta
            elif model == 'logistic':
                new_pal = old_pal + logistic(speed_factor) * (aggimpact - old_pal) * delta
            else:
                print 'Wrong value for model!'
                raw_input()
            graphs[t].node[node]['pal'][t] = new_pal
    return graphs

"""
The logistic function can be tuned by changing the steepness and threshold values. It receives a number and returns a
value between 0 and 1.
"""
def logistic(number):
    steepness = 0.3
    threshold = 20
    log_number = (1 / (1 + np.exp(-steepness * (number - threshold))) - 1 / (1 + np.exp(steepness * threshold))) * \
                (1 + np.exp(-steepness * threshold))
    return log_number



ListOfGraphs = pickle.load(open('./data_set_input/list_graphs.pickle','rb'))
g_result = run_contagion_mode(ListOfGraphs, 'weighted')