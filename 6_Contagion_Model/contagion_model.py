import numpy as np
import networkx as nx
import pandas as pd
import pickle
import datetime
import matplotlib.pyplot as plt

'''
Reading the files
    activities
    attributes
    list of graphs
'''
# activities
activities = pd.read_csv('./input_data/activities.csv')
activities['date'] = activities['date'].astype(np.datetime64)
del activities['Unnamed: 0']
activities['goal_achieved'] = activities['pal']/activities['target_pal']
activities.loc[ activities['goal_achieved']==np.inf, 'goal_achieved'] = np.nan


# Giving ints to days in activities
initial_date = pd.to_datetime('2010-04-28')

activities['initial_date'] = initial_date
activities['date'] = pd.to_datetime(activities['date'])
activities['date (days)'] = activities['date'] - activities['initial_date']
activities['date (days)'] = activities['date (days)'].astype('timedelta64[D]').astype(int)

activities.to_csv('./intermediate_data/activities.csv')

# attributes
attributes = pd.read_csv('./intermediate_data/attributes.csv')
attributes['start_plan_date'] = attributes['start_plan_date'].astype(np.datetime64)
try:
    del attributes['Unnamed: 0']
except:
    print 'error deleting one of the attributes'

nodesTimeLife = pickle.load(open('./intermediate_data/nodesTimeLife.pickle','r'))

# graphs
graphs = pickle.load(open('./input_data/list_graphs.pickle','rb'))
begin_date = np.datetime64('2010-04-28')

'''
Start running model
1 - Initialize the variables
'''
days = len(graphs)

# Running the time steps for the model
listerror = []
model = 'weighted'
delta = 1
# List of nodes that were initialized already. Every time step this list will give the information of the nodes.
# list of active nodes for this time step
active_nodes = []
inactive_nodes = []
# Start the nodes that are in_plan
for node in graphs[0].nodes():
    # Nodes on first plan have an end date for first plan after the begin date.
    if nodesTimeLife[int(node)]['start'] < 0 and nodesTimeLife[int(node)]['end'] >= 0:
        active_nodes.append(node)
        graphs[0].node[node]['activityTimeLine'] = {0: graphs[0].node[node]['initial_goal']}

# Expressivenness is 0.5 to all
exp = 0.5
# Opennesses is 0.5 when the node is active
openn = 0.5

# for t in np.arange(1,days):
for t in np.arange(1, days):
    print 'Day ', t
    graph_ant = graphs[t - 1]
    # print id(graph_ant) == id(graphs[t - 1])
    graph_cur = graphs[t]

    # We need to know which nodes were initialized this day to not try to update them
    new_nodes = []
    removed_nodes = []

    # Adding nodes that are new
    for node in graph_cur.nodes():
        start_node_date = nodesTimeLife[int(node)]['start']
        end_node_date = nodesTimeLife[int(node)]['end']

        # New node to be added. Nodes inactivated before cannot go back
        if (node not in active_nodes) and (node not in inactive_nodes):
            if (end_node_date > 0) and (start_node_date <= t):
                # print 'New node: ', node
                new_nodes.append(node)
                try:
                    graph_cur.node[str(node)]['activityTimeLine'] = {t: graph_cur.node[node]['initial_goal']}
                except:
                    print node, t
                    # raw_input()

    # Some nodes are not in the current graph because they were dropped, but they may still be at the active_nodes list
    for node in active_nodes:
        start_node_date = nodesTimeLife[int(node)]['start']
        end_node_date = nodesTimeLife[int(node)]['end']

        dropout_day = nodesTimeLife[int(node)]['dropout']

        if dropout_day <= t and dropout_day != 0:
            print 'Drop: ', node
            active_nodes.remove(node)
            removed_nodes.append(node)
            inactive_nodes.append(node)
        else:
            if (end_node_date < t) and (end_node_date > 0):
                print 'Expired: ', node
                if int(node) == 2763:
                    print 'Dia!',t
                active_nodes.remove(node)
                removed_nodes.append(node)
                inactive_nodes.append(node)

    # active_nodes contains the nodes that are in first plan, and therefore should be updated.
    for node in active_nodes:
        if node in new_nodes:
            continue
        # coefficient to calculate the aggimpact
        cs = 0.0
        aggimpact = 0.0
        speed_factor = 0.0

        num_neighbours = len(graph_ant.neighbors(node))

        # Copy the previous graph info
        # print graph_ant[node]
        dictAnt = graph_ant.node[node]['activityTimeLine']
        graph_cur.node[node]['activityTimeLine'] = dict(dictAnt)

        # The values for exp and openn in comments are the way to use a different value for each agent
        for neigh in graph_ant.neighbors(node):
            # exp = graph.node[neigh]['expressivenness']
            connect = 1  # g.get_edge_data(neigh,node).values()[0]

            # Checking if the neighbor has data
            try:
                # Take the value of the activity for this node in this day
                activity_neigh = activities.loc[(activities['id'] == int(neigh)) &
                                                (activities['date (days)'] == t), 'goal_achieved'].values[0]
                # openn = graph.node[node]['openness']
                wba = exp * connect * openn
                speed_factor = speed_factor + wba
                cs = cs + exp * connect
            except:
                # Node without data for this day
                num_neighbours = num_neighbours-1

        # Calculate the qStart (or aggimpact)
        for neigh in graph_ant.neighbors(node):
            # exp = graph.node[neigh]['expressivenness']
            connect = 1  # graph.get_edge_data(neigh, node).values()[0]
            try:
                # Take the value of the activity for this node in this day
                # v_neigh = graph_ant.node[neigh]['activityTimeLine'][t - 1]
                activity_neigh = activities.loc[(activities['id'] == int(neigh)) &
                                                (activities['date (days)'] == t), 'goal_achieved'].values[0]
            except:
                # print 'Node with Missing Data -> ' + str(neigh) + '\n'
                activity_neigh = 0
                # print graph_ant.node[neigh]['activityTimeLine']
                # raw_input()

            if cs == 0:
                aggimpact = 0
            else:
                aggimpact = aggimpact + (exp * connect * activity_neigh) / cs
        try:
            old_activity = graph_ant.node[node]['activityTimeLine'][t - 1]
        except:
            print 'Getting old activity --> ', node
            print graph_ant.node[node]
            raw_input()

        # Definition of the speed factor
        if model == 'original':
            new_activity = old_activity + speed_factor * (aggimpact - old_activity) * delta
        elif model == 'weighted':
            if num_neighbours == 0:
                new_activity = old_activity
            else:
                new_activity = old_activity + (speed_factor / num_neighbours) * (aggimpact - old_activity) * delta
        elif model == 'logistic':
            new_activity = old_activity + logistic(speed_factor) * (aggimpact - old_activity) * delta
        else:
            print 'Wrong value for model!'
            raw_input()
        graph_cur.node[node]['activityTimeLine'].update({t: new_activity})

    active_nodes = active_nodes + new_nodes

    # print 'New nodes: ', len(new_nodes)
    # print 'Removed nodes: ', len(removed_nodes)

    # print 'Total nodes: ', len(active_nodes)
    # print 'Inactive nodes: ', len(inactive_nodes)
    # print 'Nodes in the graph: ', len(graphs[t].nodes())
    print '--------------------------------'

pickle.dump(graphs, open('./intermediate_data/graphs.pickle', 'wb'))
pickle.dump(graph_cur, open('./intermediate_data/final_graph.pickle', 'wb'))


"""
The logistic function can be tuned by changing the steepness and threshold values. It receives a number and returns a
value between 0 and 1.
"""
def logistic(number):
    steepness = 0.3
    threshold = 10
    log_number = (1 / (1 + np.exp(-steepness * (number - threshold))) - 1 / (1 + np.exp(steepness * threshold))) * \
                (1 + np.exp(-steepness * threshold))
    return log_number