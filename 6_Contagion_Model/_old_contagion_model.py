"""
contagion_model.py
Created by: Eric Araujo

+++++ This file has the contagion model and this model can be run for the three modes:
        (1) Original
        (2) Weighted
        (3) Logistic
The differences are mainly at the speed factor.
This function also considers that all the agents have the same value for expressiveness and openessess.

Last change: 2016-09-30
"""

import numpy as np


"""
Model = "original" -> old
Model = "weighted" -> new with weighted
Model = "logistic" -> new with logistic
"""
def run_contagion_mode(g, parameters, model = 'original'):
    graph = g.copy()
    delta = 1.0
    steps = 30
    agents = graph.number_of_nodes()
    openn = parameters[0]
    exp = parameters[1]
    for t in np.arange(1,steps):
        for node in graph.nodes():
            # coefficient to calculate the aggimpact
            cs = 0.0
            aggimpact = 0.0
            speed_factor = 0.0
            num_neighbours = len(graph.predecessors(node))
            # Calculate speed factor
            # The values for exp and openn in comments are the way to use a different value for each agent
            for neigh in graph.predecessors(node):
                #exp = graph.node[neigh]['expressivenness']
                connect = graph.get_edge_data(neigh,node).values()[0]
                #openn = graph.node[node]['openness']
                wba = exp * connect * openn
                speed_factor = speed_factor + wba
                cs = cs +  exp * connect
            # Calculate the qStart (or aggimpact)
            for neigh in graph.predecessors(node):
                #exp = graph.node[neigh]['expressivenness']
                connect = graph.get_edge_data(neigh, node).values()[0]
                try:
                    v_neigh = graph.node[neigh]['pal'][t-1]
                except:
                    print 'Node ' + str(neigh) + '\n'
                    return
                if cs == 0:
                    aggimpact = 0
                else:
                    aggimpact = aggimpact + (exp * connect * v_neigh)/cs

            old_pal = graph.node[node]['pal'][t-1]
            # Definition of the speed factor
            if model == 'original':
                new_pal = old_pal + speed_factor * (aggimpact - old_pal)*delta
            elif model == 'weighted':
                new_pal = old_pal + (speed_factor / num_neighbours) * (aggimpact - old_pal) * delta
            elif model == 'logistic':
                new_pal = old_pal + logistic(speed_factor) * (aggimpact - old_pal) * delta
            else:
                print 'Wrong value for model!'
                return
            graph.node[node]['pal'].append(new_pal)

    return graph



"""
The logistic function can be tuned by changing the steepness and threshold values. It receives a number and returns a
value between 0 and 1.
"""
def logistic(number):
    steepness = 0.3
    threshold = 20
    log_number = (1 / (1 + np.exp(-steepness * (number - threshold))) - 1 / (1 + np.exp(steepness * threshold))) * \
                (1 + np.exp(-steepness * threshold))
    return log_number