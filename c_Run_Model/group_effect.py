import numpy as np
import pandas as pd
import pickle


coefficient = 0.0005821
# activities
activities = pd.read_csv('./clean_data/activities.csv')
activities['date'] = activities['date'].astype(np.datetime64)

# attributes
attributes = pd.read_csv('./clean_data/attributes.csv', index_col='id')
attributes['start_plan_date'] = attributes['start_plan_date'].astype(np.datetime64)
#attributes['end_plan_date (days)'] = attributes['start_plan_date (days)'] + 84

# graphs
graphs = pickle.load(open('./clean_data/list_graphs.pickle','rb'))
begin_date = np.datetime64('2010-04-28')

#Start running model
#1 - Initialize the variables

days = len(graphs)

# Running the time steps for the model
listerror = []

# Start the nodes that are in_plan
for node in graphs[0].nodes():
    # Nodes on first plan have an end date for first plan after the begin date.
    start_day = attributes.ix[int(node), 'start_plan_date (days)']
    end_day = attributes.ix[int(node), 'end_plan_date (days)']
    dropout_day = attributes.ix[int(node), 'dropout (days)']

    if (start_day <= 0) and (end_day > 0) and (dropout_day > 0):
        graphs[0].node[node]['activityTimeLine'] = {0: graphs[0].node[node]['initial_goal']}


for t in np.arange(1, days):
    print 'Day ', t
    graph_ant = graphs[t - 1]
    graph_cur = graphs[t]
    # We need to know which nodes were initialized this day to not try to update them
    active_nodes = []
    inactive_nodes = []
    new_nodes = []
    # Adding nodes that are new
    for node in graph_cur.nodes():
        start_node_date = attributes.ix[int(node), 'start_plan_date (days)']
        end_node_date = attributes.ix[int(node), 'end_plan_date (days)']
        dropout_day = attributes.ix[int(node), 'dropout (days)']

        # New node to be added. Nodes inactivated before cannot go back
        if start_node_date == t and dropout_day > t:
            try:
                graph_cur.node[str(node)]['activityTimeLine'] = {t: graph_cur.node[node]['initial_goal']}
                new_nodes.append(node)
            except:
                print node, t

        # Active nodes at the current graph
        elif start_node_date < t and end_node_date > t and dropout_day > t:
            active_nodes.append(node)
            try:
                graph_cur.node[str(node)]['activityTimeLine'] = dict(graph_ant.node[str(node)]['activityTimeLine'])
            except:
                print 'Error', node, t
                exit()

        elif dropout_day == t:
            if node in active_nodes:
                active_nodes.remove(node)
            inactive_nodes.append(node)
        else:
            inactive_nodes.append(node)

    print '\tTotal nodes: ', len(graph_cur.nodes())
    print '\tNew nodes: ', len(new_nodes), '\n\tActive nodes: ', len(active_nodes), '\n\tRemoved nodes: ', len(inactive_nodes)


    # active_nodes contains the nodes that are in first plan, and therefore should be updated.
    for node in active_nodes:
        try:
            old_activity = graph_cur.node[node]['activityTimeLine'][t - 1]
        except:
            print 'Getting old activity --> ', node
            print graph_ant.node[node]
            raw_input()


        new_activity = old_activity + coefficient
        graph_cur.node[node]['activityTimeLine'].update({t: new_activity})


    print '--------------------------------'

pickle.dump(graphs, open('./clean_data/graphs_pure_group.pickle', 'wb'))


