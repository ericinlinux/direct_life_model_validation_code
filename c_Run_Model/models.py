'''
This code contains the 3 functions for the three scenarios to run.
'''

import numpy as np
import networkx as nx
import pandas as pd
import pickle



def contagion_model(attributes=None, activities=None, model = 'weighted', delta = 1, exp = 0.5, openn = 0.5):
    # graphs
    graphs = pickle.load(open('./clean_data/list_graphs.pickle', 'rb'))
    days = len(graphs)

    # Start the nodes that are in_plan
    for node in graphs[0].nodes():
        # Nodes on first plan have an end date for first plan after the begin date.
        start_day = attributes.ix[int(node), 'start_plan_date (days)']
        end_day = attributes.ix[int(node), 'end_plan_date (days)']
        dropout_day = attributes.ix[int(node), 'dropout (days)']
        if (start_day <= 0) and (end_day > 0) and (dropout_day > 0):
            graphs[0].node[node]['activityTimeLine'] = {0: graphs[0].node[node]['initial_goal']}

    for t in np.arange(1, days):
        print 'Day ', t
        graph_ant = graphs[t - 1]
        graph_cur = graphs[t]
        # We need to know which nodes were initialized this day to not try to update them
        active_nodes = []
        inactive_nodes = []
        new_nodes = []
        # Adding nodes that are new
        for node in graph_cur.nodes():
            start_node_date = attributes.ix[int(node), 'start_plan_date (days)']
            end_node_date = attributes.ix[int(node), 'end_plan_date (days)']
            dropout_day = attributes.ix[int(node), 'dropout (days)']

            # New node to be added. Nodes inactivated before cannot go back
            if start_node_date == t and dropout_day > t:
                try:
                    graph_cur.node[str(node)]['activityTimeLine'] = {t: graph_cur.node[node]['initial_goal']}
                    new_nodes.append(node)
                except:
                    print node, t

            # Active nodes at the current graph
            elif start_node_date < t and end_node_date > t and dropout_day > t:
                active_nodes.append(node)
                try:
                    graph_cur.node[str(node)]['activityTimeLine'] = dict(graph_ant.node[str(node)]['activityTimeLine'])
                except:
                    print 'Error', node, t
                    exit()

            elif dropout_day == t:
                if node in active_nodes:
                    active_nodes.remove(node)
                inactive_nodes.append(node)
            else:
                inactive_nodes.append(node)

        print '\tTotal nodes: ', len(graph_cur.nodes())
        print '\tNew nodes: ', len(new_nodes), '\n\tActive nodes: ', len(active_nodes), '\n\tRemoved nodes: ', len(
            inactive_nodes)

        # active_nodes contains the nodes that are in first plan, and therefore should be updated.
        for node in active_nodes:
            # coefficient to calculate the aggimpact
            cs = 0.0
            aggimpact = 0.0
            speed_factor = 0.0

            num_neighbours = len(graph_ant.neighbors(node))
            # If no neighbors, then the node should keep its old value.
            if num_neighbours == 0:
                old_activity = graph_cur.node[node]['activityTimeLine'][t - 1]
                graph_cur.node[node]['activityTimeLine'].update({t: old_activity})
                continue

            # The values for exp and openn in comments are the way to use a different value for each agent
            for neigh in graph_ant.neighbors(node):
                # exp = graph.node[neigh]['expressivenness']
                connect = 1  # g.get_edge_data(neigh,node).values()[0]

                # Checking if the neighbor has data
                try:
                    # Take the value of the activity for this node in this day
                    activity_neigh = activities.loc[(activities['id'] == int(neigh)) &
                                                    (activities['date (days)'] == t), 'goal_achieved'].values[0]
                    # openn = graph.node[node]['openness']
                    wba = exp * connect * openn
                    speed_factor = speed_factor + wba
                    cs = cs + exp * connect
                except:
                    # Node without data for this day
                    num_neighbours = num_neighbours - 1

            # Calculate the qStart (or aggimpact)
            for neigh in graph_ant.neighbors(node):
                # exp = graph.node[neigh]['expressivenness']
                connect = 1  # graph.get_edge_data(neigh, node).values()[0]
                try:
                    # Take the value of the activity for this node in this day
                    # v_neigh = graph_ant.node[neigh]['activityTimeLine'][t - 1]
                    activity_neigh = activities.loc[(activities['id'] == int(neigh)) &
                                                    (activities['date (days)'] == t), 'goal_achieved'].values[0]
                except:
                    # print 'Node with Missing Data -> ' + str(neigh) + '\n'
                    activity_neigh = 0
                    # print graph_ant.node[neigh]['activityTimeLine']
                    # raw_input()

                if cs == 0:
                    aggimpact = 0
                else:
                    aggimpact = aggimpact + (exp * connect * activity_neigh) / cs
            try:
                old_activity = graph_ant.node[node]['activityTimeLine'][t - 1]
            except:
                print 'Getting old activity --> ', node
                print graph_ant.node[node]
                raw_input()

            # Definition of the speed factor
            if model == 'original':
                new_activity = old_activity + speed_factor * (aggimpact - old_activity) * delta
            elif model == 'weighted':
                if num_neighbours == 0:
                    new_activity = old_activity
                else:
                    new_activity = old_activity + (speed_factor / num_neighbours) * (aggimpact - old_activity) * delta
            else:
                print 'Wrong value for model!'
                raw_input()
            graph_cur.node[node]['activityTimeLine'].update({t: new_activity})

        active_nodes = active_nodes + new_nodes

        # print 'New nodes: ', len(new_nodes)
        # print 'Removed nodes: ', len(removed_nodes)

        # print 'Total nodes: ', len(active_nodes)
        # print 'Inactive nodes: ', len(inactive_nodes)
        # print 'Nodes in the graph: ', len(graphs[t].nodes())
        print '--------------------------------'
    return graphs

def linear_increasing(activities, attributes):
    coefficient = 0.0005821

    # graphs
    graphs = pickle.load(open('./clean_data/list_graphs.pickle', 'rb'))
    begin_date = np.datetime64('2010-04-28')

    # Start running model
    # 1 - Initialize the variables

    days = len(graphs)

    # Running the time steps for the model
    listerror = []

    # Start the nodes that are in_plan
    for node in graphs[0].nodes():
        # Nodes on first plan have an end date for first plan after the begin date.
        start_day = attributes.ix[int(node), 'start_plan_date (days)']
        end_day = attributes.ix[int(node), 'end_plan_date (days)']
        dropout_day = attributes.ix[int(node), 'dropout (days)']

        if (start_day <= 0) and (end_day > 0) and (dropout_day > 0):
            graphs[0].node[node]['activityTimeLine'] = {0: graphs[0].node[node]['initial_goal']}

    for t in np.arange(1, days):
        print 'Day ', t
        graph_ant = graphs[t - 1]
        graph_cur = graphs[t]
        # We need to know which nodes were initialized this day to not try to update them
        active_nodes = []
        inactive_nodes = []
        new_nodes = []
        # Adding nodes that are new
        for node in graph_cur.nodes():
            start_node_date = attributes.ix[int(node), 'start_plan_date (days)']
            end_node_date = attributes.ix[int(node), 'end_plan_date (days)']
            dropout_day = attributes.ix[int(node), 'dropout (days)']

            # New node to be added. Nodes inactivated before cannot go back
            if start_node_date == t and dropout_day > t:
                try:
                    graph_cur.node[str(node)]['activityTimeLine'] = {t: graph_cur.node[node]['initial_goal']}
                    new_nodes.append(node)
                except:
                    print node, t

            # Active nodes at the current graph
            elif start_node_date < t and end_node_date > t and dropout_day > t:
                active_nodes.append(node)
                try:
                    graph_cur.node[str(node)]['activityTimeLine'] = dict(graph_ant.node[str(node)]['activityTimeLine'])
                except:
                    print 'Error', node, t
                    exit()

            elif dropout_day == t:
                if node in active_nodes:
                    active_nodes.remove(node)
                inactive_nodes.append(node)
            else:
                inactive_nodes.append(node)

        print '\tTotal nodes: ', len(graph_cur.nodes())
        print '\tNew nodes: ', len(new_nodes), '\n\tActive nodes: ', len(active_nodes), '\n\tRemoved nodes: ', len(
            inactive_nodes)

        # active_nodes contains the nodes that are in first plan, and therefore should be updated.
        for node in active_nodes:
            try:
                old_activity = graph_cur.node[node]['activityTimeLine'][t - 1]
            except:
                print 'Getting old activity --> ', node
                print graph_ant.node[node]
                raw_input()
            try:
                real_target = int(activities.loc[(activities['id']==int(node)) & (activities['date (days)']==t)]['target_pal'])
            except:
                # Take the last available target pal
                real_target = find_target_pal(activities, t,node)

            if np.isnan(real_target):
                print 'got wrong real target for node ', node, ' day ', t
                real_target = graph_ant.node[node]['initial_target_pal']
                #exit(0)

            new_activity = old_activity + coefficient/real_target
            graph_cur.node[node]['activityTimeLine'].update({t: new_activity})

        active_nodes = active_nodes + new_nodes

        print '--------------------------------'

    return graphs

def find_target_pal(activities, time, node):
    for t in range(time, 0, -1):
        try:
            real_target = int(
                activities.loc[(activities['id'] == int(node)) & (activities['date (days)'] == t)]['target_pal'])
            return real_target
        except:
            continue
    print 'no target pal for node ', node, ' BEFORE time ', time

    for t in range(time, time+10):
        try:
            real_target = int(
                activities.loc[(activities['id'] == int(node)) & (activities['date (days)'] == t)]['target_pal'])
            return real_target
        except:
            continue
    print 'no target pal for node ', node, ' AFTER time ', time
    return np.nan



def combined_model(attributes, activities, model = 'weighted'):
    coefficient = 0.0005821

    # graphs
    graphs = pickle.load(open('./clean_data/list_graphs.pickle', 'rb'))
    begin_date = np.datetime64('2010-04-28')

    # Start running model
    # 1 - Initialize the variables

    days = len(graphs)

    # Running the time steps for the model
    listerror = []

    delta = 1

    # Start the nodes that are in_plan
    for node in graphs[0].nodes():
        # Nodes on first plan have an end date for first plan after the begin date.
        start_day = attributes.ix[int(node), 'start_plan_date (days)']
        end_day = attributes.ix[int(node), 'end_plan_date (days)']
        dropout_day = attributes.ix[int(node), 'dropout (days)']

        if (start_day <= 0) and (end_day > 0) and (dropout_day > 0):
            graphs[0].node[node]['activityTimeLine'] = {0: graphs[0].node[node]['initial_goal']}

    # Expressivenness is 0.5 to all
    exp = 0.5
    # Opennesses is 0.5 when the node is active
    openn = 0.5

    for t in np.arange(1, days):
        print 'Day ', t
        graph_ant = graphs[t - 1]
        graph_cur = graphs[t]
        # We need to know which nodes were initialized this day to not try to update them
        active_nodes = []
        inactive_nodes = []
        new_nodes = []
        # Adding nodes that are new
        for node in graph_cur.nodes():
            start_node_date = attributes.ix[int(node), 'start_plan_date (days)']
            end_node_date = attributes.ix[int(node), 'end_plan_date (days)']
            dropout_day = attributes.ix[int(node), 'dropout (days)']

            # New node to be added. Nodes inactivated before cannot go back
            if start_node_date == t and dropout_day > t:
                try:
                    graph_cur.node[str(node)]['activityTimeLine'] = {t: graph_cur.node[node]['initial_goal']}
                    new_nodes.append(node)
                except:
                    print node, t

            # Active nodes at the current graph
            elif start_node_date < t and end_node_date > t and dropout_day > t:
                active_nodes.append(node)
                try:
                    graph_cur.node[str(node)]['activityTimeLine'] = dict(graph_ant.node[str(node)]['activityTimeLine'])
                except:
                    print 'Error', node, t
                    exit()

            elif dropout_day == t:
                if node in active_nodes:
                    active_nodes.remove(node)
                inactive_nodes.append(node)
            else:
                inactive_nodes.append(node)

        print '\tTotal nodes: ', len(graph_cur.nodes())
        print '\tNew nodes: ', len(new_nodes), '\n\tActive nodes: ', len(active_nodes), '\n\tRemoved nodes: ', len(
            inactive_nodes)

        # active_nodes contains the nodes that are in first plan, and therefore should be updated.
        for node in active_nodes:
            # coefficient to calculate the aggimpact
            cs = 0.0
            aggimpact = 0.0
            speed_factor = 0.0

            num_neighbours = len(graph_ant.neighbors(node))
            # If no neighbors, then the node should keep its old value.
            if num_neighbours == 0:
                old_activity = graph_cur.node[node]['activityTimeLine'][t - 1] + coefficient
                graph_cur.node[node]['activityTimeLine'].update({t: old_activity})
                continue

            # The values for exp and openn in comments are the way to use a different value for each agent
            for neigh in graph_ant.neighbors(node):
                # exp = graph.node[neigh]['expressivenness']
                connect = 1  # g.get_edge_data(neigh,node).values()[0]

                # Checking if the neighbor has data
                try:
                    # Take the value of the activity for this node in this day
                    activity_neigh = activities.loc[(activities['id'] == int(neigh)) &
                                                    (activities['date (days)'] == t), 'goal_achieved'].values[0]
                    # openn = graph.node[node]['openness']
                    wba = exp * connect * openn
                    speed_factor = speed_factor + wba
                    cs = cs + exp * connect
                except:
                    # Node without data for this day
                    num_neighbours = num_neighbours - 1

            # Calculate the qStart (or aggimpact)
            for neigh in graph_ant.neighbors(node):
                # exp = graph.node[neigh]['expressivenness']
                connect = 1  # graph.get_edge_data(neigh, node).values()[0]
                try:
                    # Take the value of the activity for this node in this day
                    # v_neigh = graph_ant.node[neigh]['activityTimeLine'][t - 1]
                    activity_neigh = activities.loc[(activities['id'] == int(neigh)) &
                                                    (activities['date (days)'] == t), 'goal_achieved'].values[0]
                except:
                    # print 'Node with Missing Data -> ' + str(neigh) + '\n'
                    activity_neigh = 0
                    # print graph_ant.node[neigh]['activityTimeLine']
                    # raw_input()

                if cs == 0:
                    aggimpact = 0
                else:
                    aggimpact = aggimpact + (exp * connect * activity_neigh) / cs
            try:
                old_activity = graph_ant.node[node]['activityTimeLine'][t - 1]
            except:
                print 'Getting old activity --> ', node
                print graph_ant.node[node]
                raw_input()

            # Definition of the speed factor
            if model == 'original':
                new_activity = old_activity + speed_factor * (aggimpact - old_activity) * delta
            elif model == 'weighted':
                if num_neighbours == 0:
                    new_activity = old_activity
                else:
                    new_activity = (old_activity) + (speed_factor / num_neighbours) * (
                    aggimpact - old_activity) * delta
            else:
                print 'Wrong value for model!'
                raw_input()


            try:
                real_target = int(activities.loc[(activities['id']==int(node)) & (activities['date (days)']==t)]['target_pal'])
            except:
                # Take the last available target pal
                real_target = find_target_pal(activities, t,node)

            if np.isnan(real_target):
                print 'got wrong real target for node ', node, ' day ', t
                real_target = graph_ant.node[node]['initial_target_pal']
                #exit(0)

            new_activity = new_activity + coefficient/real_target

            graph_cur.node[node]['activityTimeLine'].update({t: new_activity})

        active_nodes = active_nodes + new_nodes

        # print 'New nodes: ', len(new_nodes)
        # print 'Removed nodes: ', len(removed_nodes)

        # print 'Total nodes: ', len(active_nodes)
        # print 'Inactive nodes: ', len(inactive_nodes)
        # print 'Nodes in the graph: ', len(graphs[t].nodes())
        print '--------------------------------'
    return graphs


if __name__ == '__main__':

    # activities of the participants
    activities = pd.read_csv('./clean_data/activities.csv')
    activities['date'] = activities['date'].astype(np.datetime64)

    # attributes
    attributes = pd.read_csv('./clean_data/attributes.csv', index_col='id')
    attributes['start_plan_date'] = attributes['start_plan_date'].astype(np.datetime64)


    graphs = contagion_model(attributes=attributes, activities=activities)
    pickle.dump(graphs, open('./clean_data/graphs_pure_model.pickle', 'wb'))

    #graphs = linear_increasing(attributes=attributes, activities=activities)
    #pickle.dump(graphs, open('./clean_data/graphs_linear_increasing.pickle', 'wb'))

    graphs = combined_model(attributes=attributes, activities=activities)
    pickle.dump(graphs, open('./clean_data/graphs_merged.pickle', 'wb'))

