'''
This could intends to join the process of cleaning and setting the tables in one step. It is a new approach.
Programmer: Eric Araujo
Last update: 2016-11-02
'''

import xml.etree.ElementTree as ET
import networkx as nx
import datetime
import os
import pandas as pd

def clean_nodes(gexf = "./raw_data/communalitics2015_dynamical.gexf", pals = "./raw_data/activity_per_day.csv"):
    print '--------------------------------------------------------'
    print 'Clean nodes function...'
    print '--------------------------------------------------------'

    tree = ET.ElementTree()
    tree.parse(gexf)
    root = tree.getroot()
    graph = root.find('{"http://www.gexf.net/1.2draft}graph')

    # Get nodes reference
    nodes = graph.find('{"http://www.gexf.net/1.2draft}nodes')
    nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')

    print 'Starting cleaning of nodes...'
    print 'Initial nodes: #' + str(len(nodeList))

    ## Remove nodes without any attribute
    print '\na) Remove nodes without any attribute...'

    nodesWithoutAttr = []
    for node in nodeList:
        attvalues = node.find('{"http://www.gexf.net/1.2draft}attvalues')
        if attvalues == None:
            nodesWithoutAttr.append(node.attrib['id'])
            nodes.remove(node)

    print 'Nodes removed (a): #' + str(len(nodesWithoutAttr))

    # Refreshing the information
    nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')
    print 'Nodes remaining: #' + str(len(nodeList))

    # Removing nodes without start_date attribute
    print '\nb) Remove nodes without start date attribute...'

    nodesWithoutStartDate = []
    for node in nodeList:
        rootAttvalues = node.find('{"http://www.gexf.net/1.2draft}attvalues')
        lstAttvalues = rootAttvalues.findall('{"http://www.gexf.net/1.2draft}attvalue')
        for att in lstAttvalues:
            if att.get('for') == '1':
                if att.get('value') == "":
                    nodesWithoutStartDate.append(node.attrib['id'])
                    nodes.remove(node)
                    continue

    print 'Nodes removed (b): #' + str(len(nodesWithoutStartDate))

    # Refresh the list of nodes
    nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')
    print 'Nodes remaining: #' + str(len(nodeList))


    print '\nc) Remove nodes with dropout date before 28/04/2010...'

    nodesDropedBeforeStartDate = []
    start_date = datetime.datetime(2010,4,28)

    for node in nodeList:
        rootAttvalues = node.find('{"http://www.gexf.net/1.2draft}attvalues')
        lstAttvalues = rootAttvalues.findall('{"http://www.gexf.net/1.2draft}attvalue')
        for att in lstAttvalues:
            if att.get('for') == '2':
                if att.get('value') == "":
                    continue
                else:
                    dropoutDate = datetime.datetime.strptime(att.get('value'), '%Y-%m-%d')
                    if dropoutDate < start_date:
                        nodesDropedBeforeStartDate.append(node.attrib['id'])
                        nodes.remove(node)
                        continue

    print 'Nodes removed (c): #' + str(len(nodesDropedBeforeStartDate))

    # Refresh the list of nodes
    nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')
    print 'Nodes remaining: #' + str(len(nodeList))

    # ----------------------------------------------------------------------------------------------------
    print '\nd) Cleaning csv file and conflicts...'

    jointNodes2Remove = nodesWithoutAttr + nodesWithoutStartDate + nodesDropedBeforeStartDate
    print 'Nodes to be removed from csv file: ' + str(len(jointNodes2Remove))

    newcsv = open("./intermediate_data/activities_temp.csv", 'w')

    # Open activitites csv file
    csvFile = open(pals, 'r')

    # Create list of nodes in the gexf file. The rest is removed from csv file.
    listOfNodes = []
    for node in nodeList:
        listOfNodes.append(int(node.attrib['id']))

    numlines=0


    for line in csvFile:
        numlines+=1
        node = line.split(',')[0]
        if node == 'id':
            newcsv.write(line)
            continue
        else:
            if int(node) in listOfNodes:
                newcsv.write(line)
    print '\n Saving new csv file in ./intermediate_data/activities_temp.csv...'
    newcsv.close()

    print '\nNumber of lines originally at the csv file: ', numlines
    # ---------------------------------------------------------------------------------------
    datacsv = "./intermediate_data/activities_temp.csv"

    print 'e) Starting cleaning of nodes from graph that don\'t have data at csv...'

    print '\nNumber of nodes in the graph: ' + str(len(nodeList))

    activities = pd.read_csv(datacsv)
    listOfNodesCSV = list(set(activities['id']))

    print '\nNumber of nodes at CSV file: ', len(listOfNodesCSV)

    removedNodes = []
    for node in nodeList:
        n_id = node.attrib['id']
        if int(n_id) not in listOfNodesCSV:
            removedNodes.append(node.attrib['id'])
            nodes.remove(node)
            continue

    print '\nNodes removed: ', len(removedNodes)

    print '\nSaving new gefx file without the nodes in ./intermediate_data/graphWithoutNodes.gexf...'
    tree.write('./intermediate_data/graphWithoutNodes.gexf')




def clean_edges(gexf = './intermediate_data/graphWithoutNodes.gexf'):
    print '--------------------------------------------------------'
    print 'Clean Edges function...'
    print '--------------------------------------------------------'

    tree = ET.ElementTree()
    tree.parse(gexf)
    root = tree.getroot()

    graph = root.find('{"http://www.gexf.net/1.2draft}graph')
    edges = graph.find('{"http://www.gexf.net/1.2draft}edges')
    edgeList = edges.findall('{"http://www.gexf.net/1.2draft}edge')

    # Get nodes reference
    nodes = graph.find('{"http://www.gexf.net/1.2draft}nodes')
    nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')

    print 'Starting cleaning of nodes...'
    print 'Initial nodes: #' + str(len(nodeList))

    listOfNodes = []
    for node in nodeList:
        listOfNodes.append(int(node.attrib['id']))

    print '\nOriginal amount of edges (including duplicated): ', len(edgeList)

    print '\na) Remove edges without attributes...'
    edgesWithoutAttr = []
    for edge in edgeList:
        attvalues = edge.find('{"http://www.gexf.net/1.2draft}attvalues')
        if attvalues == None:
            edgesWithoutAttr.append(edge.attrib['id'])
            edges.remove(edge)

    print 'Edges removed: ', len(edgesWithoutAttr)

    # Refresh the edges
    edgeList = edges.findall('{"http://www.gexf.net/1.2draft}edge')
    print '\nRemaining edges (including duplicated): ', len(edgeList)
    print '\nb) Remove edges with removed nodes as source or target...'

    edgesWithNodesRemoved = []
    for edge in edgeList:
        source = edge.get('source')
        target = edge.get('target')
        if (int(source) not in listOfNodes) or (int(target) not in listOfNodes):
            edgesWithNodesRemoved.append(edge.attrib['id'])
            edges.remove(edge)

    print '\nEdges removed: ', len(edgesWithNodesRemoved)

    edgeList = edges.findall('{"http://www.gexf.net/1.2draft}edge')
    print '\nRemaining edges (including duplicated): ', len(edgeList)

    totalRemovedEdges = edgesWithoutAttr + edgesWithNodesRemoved
    print '\nTotal edges removed: ', len(totalRemovedEdges)

    print '\nSaving new gefx file without the edges in ./intermediate_data/graphWithoutEdges.gexf...'
    tree.write('./intermediate_data/graphWithoutEdges.gexf')

'''
Remove isolated nodes
'''
def remove_isolated_nodes(gexf = "./intermediate_data/graphWithoutEdges.gexf", activities = "./intermediate_data/activities_temp.csv"):
    print '--------------------------------------------------------'
    print 'Remove isolated nodes function...'
    print '--------------------------------------------------------'

    tree = ET.ElementTree()
    tree.parse(gexf)
    root = tree.getroot()

    graph = root.find('{"http://www.gexf.net/1.2draft}graph')
    nodes = graph.find('{"http://www.gexf.net/1.2draft}nodes')
    nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')
    edges = graph.find('{"http://www.gexf.net/1.2draft}edges')
    edgeList = edges.findall('{"http://www.gexf.net/1.2draft}edge')

    print 'Initial nodes: #' + str(len(nodeList))
    print '\nOriginal amount of edges (including duplicated): ', len(edgeList)

    # Build a graph (without temporal dimension)
    g = nx.Graph()

    for node in nodes:
        node_id = node.attrib['id']
        if node_id != "":
            g.add_node(node_id)

    print 'Nodes included: #', len(g.nodes())

    repeated_edges = []
    for edge in edges:
        edge_id = edge.get('id')
        edge_source = edge.get('source')
        edge_target = edge.get('target')
        if g.has_edge(edge_target, edge_source):
            repeated_edges.append(edge_id)
        g.add_edge(edge_source, edge_target)

    print 'Edges included: #', len(g.edges())
    print 'Repeated edges: #', len(repeated_edges)

    # Getting the isolated nodes
    isolated_node = []
    for node in g.nodes():
        neigh = g.neighbors(node)
        if len(neigh) == 0:
            isolated_node.append(node)

    print 'Nodes isolated: ', len(isolated_node)

    # Removing the nodes and generating the final files
    csvOutput = './intermediate_data/activities.csv'
    gexfOutput = './intermediate_data/dynamic_network.gexf'


    # Get nodes reference
    nodes = graph.find('{"http://www.gexf.net/1.2draft}nodes')
    nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')

    print 'Starting cleaning of nodes...'
    print 'Initial nodes: #' + str(len(nodeList))

    ## Remove nodes on the isolated_nodes list
    print '\na) Remove nodes at the list of isolated nodes from gexf file...'

    for node in nodeList:
        node_id = node.attrib['id']
        if node_id in isolated_node:
            nodes.remove(node)
            continue

    nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')
    print 'Nodes remaining: #' + str(len(nodeList))

    print '\nb) Cleaning csv file and conflicts...'

    newcsv = open(csvOutput, 'w')
    activitiesCSV = open(activities, 'r')

    for line in activitiesCSV:
        node = line.split(',')[0]
        if node == 'id':
            newcsv.write(line)
            continue
        else:
            if node not in isolated_node:
                newcsv.write(line)

    print '\n Saving new csv file in ' + str(csvOutput)
    newcsv.close()

    print 'Saving the new gexf file ' + str(gexfOutput) + '...'
    tree.write(gexfOutput)


if __name__ == '__main__':

    if not os.path.exists('./intermediate_data'):
        os.makedirs('intermediate_data')
    if not os.path.exists('./clean_data'):
        os.makedirs('clean_data')
    clean_nodes()
    clean_edges()

    remove_isolated_nodes()