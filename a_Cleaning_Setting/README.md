# Cleaning and setting basic files for the model running


## Running cleaning_and_setting_data.py
```
--------------------------------------------------------
Clean nodes function...
--------------------------------------------------------
Starting cleaning of nodes...
Initial nodes: #4989

a) Remove nodes without any attribute...
Nodes removed (a): #1419
Nodes remaining: #3570

b) Remove nodes without start date attribute...
Nodes removed (b): #195
Nodes remaining: #3375

c) Remove nodes with dropout date before 28/04/2010...
Nodes removed (c): #276
Nodes remaining: #3099

d) Cleaning csv file and conflicts...
Nodes to be removed from csv file: 1890

 Saving new csv file in ./intermediate_data/activities_temp.csv...
e) Starting cleaning of nodes from graph that don't have data at csv...

Number of nodes in the graph: 3099

Number of nodes at CSV file:  3413

Nodes removed:  58

Saving new gefx file without the nodes in ./intermediate_data/graphWithoutNodes.gexf...
--------------------------------------------------------
Clean Edges function...
--------------------------------------------------------
Starting cleaning of nodes...
Initial nodes: #3041

Original amount of edges (including duplicated):  28418

a) Remove edges without attributes...
Edges removed:  3802

Remaining edges (including duplicated):  24616

b) Remove edges with removed nodes as source or target...

Edges removed:  9936

Remaining edges (including duplicated):  14680

Total edges removed:  13738

Saving new gefx file without the edges in ./intermediate_data/graphWithoutEdges.gexf...
--------------------------------------------------------
Remove isolated nodes function...
--------------------------------------------------------
Initial nodes: #3041

Original amount of edges (including duplicated):  14680
Nodes included: # 3041
Edges included: # 7337
Repeated edges: # 7343
Nodes isolated:  521
Starting cleaning of nodes...
Initial nodes: #3041

a) Remove nodes at the list of isolated nodes from gexf file...
Nodes remaining: #2520

b) Cleaning csv file and conflicts...

 Saving new csv file in ./clean_data/activities.csv
Saving the new gexf file ./clean_data/dynamic_network.gexf...
```

