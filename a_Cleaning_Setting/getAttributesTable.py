import pandas as pd
import xmltodict
import matplotlib.pyplot as plt
import os
import datetime
import xml.etree.ElementTree as ET
import numpy as np


def getAttributesTable(gexf = "./intermediate_data/dynamic_network.gexf"):
    # Converting to Gexf to Dict
    with open(gexf) as fd:
        root = xmltodict.parse(fd.read())

    graph = root['ns0:gexf']['ns0:graph']
    nodes = graph['ns0:nodes']

    print 'Number of nodes in the graph: ', len(nodes['ns0:node'])

    listofNodes = nodes['ns0:node']

    # Collecting attributes from the graph and generating a new dictionary with that
    dictNodesStartPlan = dict()
    for node in listofNodes:
        start_plan_date = ""
        dropout = ""
        gender = ""
        corp = ""
        country = ""
        bmi = ""
        nodeid = node['@id']
        #print nodeid
        listofAttributes = node['ns0:attvalues']['ns0:attvalue']
        for attrib in listofAttributes:
            (at,value) = attrib.values()
            if int(at) == 1:
                start_plan_date = value
            elif int(at) == 2:
                dropout = value
            elif int(at) == 3:
                gender = value
            elif int(at) == 4:
                corp = value
            elif int(at) == 5:
                country = value
            elif int(at) == 6:
                bmi = value
        d = {'start_plan_date': start_plan_date, 'dropout':dropout, 'gender':gender,
             'corp':corp, 'country':country, 'bmi':bmi}
        dictNodesStartPlan[nodeid] = d

    # Convert to DataFrame
    df = pd.DataFrame(dictNodesStartPlan)
    df = df.T
    df['id'] = df.index

    initial_date = pd.to_datetime('2010-04-28')
    df['initial_date'] = initial_date

    df['dropout'] = pd.to_datetime(df['dropout'])

    df['dropout (days)'] = df['dropout'] - df['initial_date']
    df.loc[pd.isnull(df['dropout (days)']),'dropout (days)'] = pd.to_timedelta('-10 days')
    df['dropout (days)'] = df['dropout (days)'].astype('timedelta64[D]').astype(int)
    df.loc[df['dropout (days)']==-10, 'dropout (days)'] = 300


    df['start_plan_date'] = pd.to_datetime(df['start_plan_date'])
    df['start_plan_date (days)'] = df['start_plan_date'] - df['initial_date']
    df['start_plan_date (days)'] = df['start_plan_date (days)'].astype('timedelta64[D]').astype(int)

    del df['initial_date']


    # Graphic
    # Take only the lines where dropout is not null!
    rows_dropout = df[~pd.isnull(df['dropout'])]
    days_drop = rows_dropout['dropout (days)']
    bins = range(max(days_drop)+1)
    plt.figure(figsize=(20,12))
    plt.title('Drop out days distribution', fontsize=28)
    plt.xlabel('Day', fontsize=20)
    plt.ylabel('Quantity (nodes)', fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.hist(days_drop.values, bins=bins)
    plt.savefig('./imgs/dropout_histogram.png')

    print '\nSaving ./clean_data/attributes.csv...'
    df.to_csv('./intermediate_data/attributes.csv')



def includeColumnsActivities(activities_csv = "./intermediate_data/activities.csv", attributes_csv = "./intermediate_data/attributes.csv"):
    activities = pd.read_csv(activities_csv)
    listOfNodes = list(set(activities['id']))
    print 'Number of nodes: ', len(listOfNodes)

    activities['goal_achieved'] = activities['pal']/activities['target_pal']
    activities.loc[activities['goal_achieved'] == np.inf, 'goal_achieved'] = np.nan

    # Giving ints to days in activities
    initial_date = pd.to_datetime('2010-04-28')

    activities['initial_date'] = initial_date
    activities['date'] = pd.to_datetime(activities['date'])
    activities['date (days)'] = activities['date'] - activities['initial_date']
    activities['date (days)'] = activities['date (days)'].astype('timedelta64[D]').astype(int)

    # Checking if both sets are the same (csv and gexf)
    attributes = pd.read_csv(attributes_csv)

    diff = len(attributes) - len(listOfNodes)
    if diff == 0:
        print 'Same number of nodes between dictionary and the csv file'
    else:
        print 'Error! CSV and Dictionary have different number of nodes!'
        exit(0)

    ###### Insert first plan tag on the nodes
    attributes = attributes.set_index('id')
    #attributes.index = attributes['id']

    for node in attributes.index:
        first_plan_date = attributes.loc[node, 'start_plan_date (days)']
        end_first_plan_date = first_plan_date + 84
        # Changing the status at activities DataFrame
        activities.loc[(activities['id'] == int(node)) & (activities['date (days)'] < end_first_plan_date) &
                       (activities['date (days)'] >= first_plan_date) &
                       (activities['status'] == 'in_plan'), 'status'] = 'first_plan'


    print '\nSaving ./intermediate_data/activities.csv...'
    activities.to_csv('./intermediate_data/activities.csv')

'''
Getting initial goal achievement for all the nodes
'''
def getInitialGoalAchieved(attributescsv = './intermediate_data/attributes.csv', actcsv = './intermediate_data/activities.csv', gexf = "./intermediate_data/dynamic_network.gexf"):
    activities = pd.read_csv(actcsv)
    attributes = pd.read_csv(attributescsv, index_col='id')

    mean_status = activities[['id', 'pal', 'target_pal', 'status']].groupby(['id', 'status']).mean()

    print '\nNodes without before_plan data:'
    for node in attributes.index:
        size = len(activities[(activities['id'] == int(node)) & (activities['status'] == 'before_plan')])
        if size < 4:
            print node, size
    # Getting a dictionary with the mean pal for before_plan
    print '\nBuilding a dictionary for the before plan data...'

    before_plan_mean = {}
    for node in attributes.index:
        try:
            pal = float(mean_status[(mean_status.index.get_level_values('id') == int(node)) &
                                    (mean_status.index.get_level_values('status') == 'before_plan')]['pal'][0])
        except:
            pal = np.nan
        before_plan_mean[node] = pal

    # It releases nodes 10409, 17149 and 28701
    activities['date'] = pd.to_datetime(activities['date'])
    # Manual change for node 28701
    activities.loc[(activities['id'] == 28701) &
                   (activities['date'] > datetime.datetime(2010, 4, 1)), 'status'] = 'in_plan'
    activities[(activities['id'] == 28701) & (activities['date'] > datetime.datetime(2010, 4, 1))].head()
    # Nodes 10409 and 17149 have start plan date before 02/2009. That means that they won't affect the results.

    ## Setting up the new column with the dictionary
    attributes['before_plan_mean'] = attributes.from_dict(before_plan_mean, orient='index')
    ###### print attributes.head()

    # ==> Getting the target_pal for first plan
    # Getting a dictionary with the mean pal for before_plan
    print '\nBuilding a dictionary for target pal...'

    target_pal = {}
    print '\nNodes without the target_pal on the first plan date: '
    for node in attributes.index:
        first_plan_date = attributes.loc[node, 'start_plan_date']
        dt_first = datetime.datetime.strptime(first_plan_date, '%Y-%m-%d')
        try:
            targ_pal = float(activities.loc[(activities['id'] == int(node)) &
                                            (activities['date'] == dt_first), 'target_pal'])
        except:
            targ_pal = -1
            print node
        target_pal[node] = targ_pal

    treat_list = [int(k) for k, v in target_pal.items() if v == -1]
    target_pal['65529'] = 1.5625
    target_pal['10409'] = -2
    target_pal['8985'] = -2

    # Setting up column for initial target pal with the dictionary
    attributes['initial_target_pal'] = attributes.from_dict(target_pal, orient='index')

    # Initial goal achieved built
    attributes['initial_goal_achieved'] = attributes['before_plan_mean'] / attributes['initial_target_pal']

    #del attributes['Unnamed: 0']
    attributes['id'] = attributes.index

    discard_list = list(attributes.loc[attributes['initial_target_pal'] == -1]['id'])
    print len(discard_list), ' nodes to be discarded.'


    ### Cleaning up the activities file
    print '\nCleaning activities file...'
    out_activities = './clean_data/activities.csv'
    newcsv = open(out_activities, 'w')
    activitiesCSV = open(actcsv, 'r')

    for line in activitiesCSV:
        node = line.split(',')[1]
        if node == 'id' or node == 'Unnamed: 0':
            newcsv.write(line)
            continue
        else:
            if int(node) not in discard_list:
                newcsv.write(line)

    print '\n Saving new csv file in ' + str(out_activities)
    newcsv.close()


    ############################################################
    # Saving the csv file with the new attributes
    ############################################################

    ### Cleaning up the attributes file
    print '\nCleaning attributes file...'

    for node in discard_list:
        attributes = attributes[attributes['id'] != node]

    attributes['end_plan_date (days)'] = attributes['start_plan_date (days)'] + 84

    print 'Saving attributes in ./intermediate_data/attributes.csv'
    del attributes['Unnamed: 0']
    attributes.to_csv("./clean_data/attributes.csv")

    ### Cleaning up the gexf file

    tree = ET.ElementTree()
    tree.parse(gexf)
    root = tree.getroot()

    graph = root.find('{"http://www.gexf.net/1.2draft}graph')
    nodes = graph.find('{"http://www.gexf.net/1.2draft}nodes')
    nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')

    edges = graph.find('{"http://www.gexf.net/1.2draft}edges')
    edgeList = edges.findall('{"http://www.gexf.net/1.2draft}edge')

    print 'Initial nodes: #' + str(len(nodeList))
    print '\nOriginal amount of edges (including duplicated): ', len(edgeList)


    ## Remove nodes on the isolated_nodes list
    print '\na) Remove nodes at the list of discard nodes from gexf file...'

    for node in nodeList:
        node_id = node.attrib['id']
        if int(node_id) in discard_list:
            nodes.remove(node)
            continue

    nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')
    print 'Nodes remaining: #' + str(len(nodeList))

    print '\nb) Remove edges with removed nodes as source or target...'

    edgesWithNodesRemoved = []
    for edge in edgeList:
        source = edge.get('source')
        target = edge.get('target')
        if int(source) in discard_list or int(target) in discard_list:
            edgesWithNodesRemoved.append(edge.attrib['id'])
            edges.remove(edge)

    print '\nEdges removed: ', len(edgesWithNodesRemoved)

    edgeList = edges.findall('{"http://www.gexf.net/1.2draft}edge')
    print '\nRemaining edges (including duplicated): ', len(edgeList)
    gexf_out = './clean_data/dynamic_network.gexf'
    print '\nSaving new gefx file in ' + gexf_out
    tree.write(gexf_out)


if __name__ == '__main__':
    if not os.path.exists('./imgs'):
        os.makedirs('imgs')

    getAttributesTable()
    includeColumnsActivities()
    getInitialGoalAchieved()