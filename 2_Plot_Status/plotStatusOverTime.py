# The aim of this file is to provide the functions to plot the status of the nodes over time.
# Created: 2016-10-13
# Eric Fernandes de Mello Araujo

# Not safe to use

import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
from matplotlib import dates
import matplotlib.dates as mdates
from matplotlib import style
style.use('fivethirtyeight')

#filename = "pre_activity_per_day.csv"
# No duplicated data
filename_new = "./input_data/activities.csv"

pdPAL = pd.read_csv(filename_new,index_col='id')
del pdPAL['Unnamed: 0']
# Convert status to numerical representation
# before_plan = 0
# in_plan = 1
# stay_active = 2
status_ordered = ['before_plan', 'first_plan', 'in_plan', 'stay_active']

pdPAL.status = pdPAL.status.astype("category",
                                   ordered=True,
                                   categories=status_ordered).cat.codes

pdPAL['date'] = pd.to_datetime(pdPAL['date'])

# Group PALs by date and status
groupPAL = pdPAL.groupby(['date','status'])

# Getting the numbers
statusDatePAL = groupPAL.count()['pal']
totalDatePAL = pdPAL.groupby(['date']).count()['pal']

dictCount = dict(statusDatePAL)
dictCountTotal = dict(totalDatePAL)

before = []
first = []
inplan = []
stayactive = []
total = []


for (time, c) in dictCountTotal.items():
    total.append((time, c))

for (time, status), c in dictCount.items():
    #print 'Time: ' + str(time) + '\tStatus: ' + str(status) + '\tCount: ' + str(c)
    if status == 0:
        before.append((time, c))
    elif status == 1:
        first.append((time,c))
    elif status == 2:
        inplan.append((time,c))
    elif status == 3:
        stayactive.append((time,c))
    else:
        print 'error: no status information!'

sorted_before = sorted(before, key=lambda tup: tup[0])
sorted_first = sorted(first, key=lambda tup: tup[0])
sorted_inplan = sorted(inplan, key=lambda tup: tup[0])
sorted_stayactive = sorted(stayactive, key=lambda tup: tup[0])
sorted_total = sorted(total, key=lambda tup: tup[0])

# Plotting
years = mdates.YearLocator()   # every year
months = mdates.MonthLocator()  # every month
monthsFmt = mdates.DateFormatter('%m-%Y')

fig = plt.figure()
ax = fig.add_subplot(111)

# Plot 1
ax.plot(*zip(*sorted_before))
ax.plot(*zip(*sorted_first))
ax.plot(*zip(*sorted_inplan))
ax.plot(*zip(*sorted_stayactive))
ax.plot(*zip(*sorted_total))

ax.xaxis.set_major_locator(months)
ax.xaxis.set_major_formatter(monthsFmt)
ax.xaxis.set_minor_locator(months)

ax.format_xdata = mdates.DateFormatter('%Y-%m-%d')
#dstart = datetime(2008,10,15)
#dfinal = datetime(2010,8,6)

dstart = datetime(2009,8,1)
dfinal = datetime(2010,7,17)
ax.set_xlim(dstart,dfinal)

ax.set_ylabel('Number of Participants')
ax.set_title('Number of participants in each \nstatus over time')


# Plot 2
# ax2 = fig.add_subplot(122)
#
#
# dictStatus = {'before': dict(sorted_before), 'first' : dict(sorted_first),
#               'in_plan' : dict(sorted_inplan), 'stay' : dict(sorted_stayactive)}
#
# df = pd.DataFrame(dictStatus)
# df.fillna(0,inplace=True)
# dates = df.index
# df.index = range(len(df.index))
# #df.plot(kind='bar', stacked=True, edgecolor = "none", figsize=(15,10), legend=True)
#
# ax2 = df[['before','first', 'in_plan', 'stay']].plot(kind='bar', stacked=True, edgecolor = "none", title ="Participants per status",figsize=(15,10),legend=True, fontsize=12)

ax.set_xlabel("Day",fontsize=12)

fig.autofmt_xdate()

plt.legend(['Before Plan', 'First Plan', 'In Plan', 'Stay Active', 'Total'], loc='upper left', fontsize=12)

fig.savefig('./imgs/statusHistory.png', bbox_inches='tight')


plt.show()
