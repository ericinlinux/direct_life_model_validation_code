import pandas as pd
import pickle
import datetime
import numpy as np


activities_csv = "./input_data/activities.csv"
attributes_pickle = "./intermediate_data/attributes.pickle"


activities = pd.read_csv(activities_csv)
listOfNodes = list(set(activities['id']))
activities['date'] = pd.to_datetime(activities['date'])
print 'Number of nodes: ', len(listOfNodes)

# Checking if both sets are the same (csv and gexf)
attributes = pickle.load(open(attributes_pickle, 'rb'))

diff = len(attributes) - len(listOfNodes)
if diff == 0:
    print 'Same number of nodes between dictionary and the csv file'
else:
    print 'Error! CSV and Dictionary have different number of nodes!'
    exit()

###### Insert first plan tag on the nodes

for node in attributes.index:
    first_plan_date = attributes.loc[node,'start_plan_date']
    dt_first = datetime.datetime.strptime(first_plan_date, '%Y-%m-%d')
    dt_end_first = dt_first + datetime.timedelta(days=84)
    end_first_plan_date = dt_end_first.strftime('%Y-%m-%d')
    # Changing the status at activities DataFrame
    activities.loc[(activities['id'] == int(node)) & (activities['date']<end_first_plan_date) &
                   (activities['date']>=first_plan_date) &
                   (activities['status'] == 'in_plan'),'status']= 'first_plan'

############################################################
### Saving the new activities csv file
############################################################
print 'Saving ./intermediate_data/activities.csv...'
activities.to_csv('./intermediate_data/activities.csv')

###########################################################################
## Getting the initial goal achieved for all nodes
###########################################################################
# Calculated by the average pal for the week before_plan divided by the target_pal of the first week on the first plan.

mean_status = activities[['id','pal','target_pal','status']].groupby(['id','status']).mean()

print '\nNodes without before_plan data:'
for node in attributes.index:
    size = len(activities[(activities['id']==int(node)) & (activities['status']=='before_plan')])
    if size < 4:
        print node, size


# Getting a dictionary with the mean pal for before_plan
print '\nBuilding a dictionary for the before plan data...'

before_plan_mean = {}
for node in attributes.index:
    try:
        pal = float(mean_status[(mean_status.index.get_level_values('id') == int(node)) &
                              (mean_status.index.get_level_values('status')=='before_plan')]['pal'][0])
    except:
        pal = np.nan
    before_plan_mean[node] = pal


# Manual change for node 28701
activities.loc[(activities['id'] == 28701) &
               (activities['date'] > datetime.datetime(2010,4,1)), 'status']= 'in_plan'
activities[(activities['id'] == 28701) & (activities['date'] > datetime.datetime(2010,4,1))].head()

## Setting up the new column with the dictionary
attributes['before_plan_mean'] = attributes.from_dict(before_plan_mean, orient='index')


#### Getting the target_pal for first plan
# Getting a dictionary with the mean pal for before_plan
print '\nBuilding a dictionary for target pal...'

target_pal = {}
print '\nNodes without the target_pal on the first plan date: '
for node in attributes.index:
    first_plan_date = attributes.loc[node,'start_plan_date']
    dt_first = datetime.datetime.strptime(first_plan_date, '%Y-%m-%d')
    try:
        targ_pal = float(activities.loc[(activities['id']==int(node)) &
           (activities['date'] == dt_first), 'target_pal'])
    except:
        targ_pal = -1
        print node
    target_pal[node] = targ_pal


'''
Treating the exceptions

* 65529: manual setting: **target pal is 1.5625.**
* 10409: this node will be only kept for influences over others. It doesn't say when is the first plan for this node, because its start_plan_date is from 2007
* 8985: this node will be only kept for influences over others. It doesn't say when is the first plan for this node, because its start_plan_date is from 2008
* 29849: this node has a start_plan_date in 2010-05-31, but its first data after before_plan is over is on 2010-06-18. For this node, it is necessary to **change the dates**. As it has only 47 days of first_plan, it is a candidate to be removed. **work to do!**
* 11507: this node has only before_plan information. **Should be discarded.**
* 30121: this node has only before_plan information. **Should be discarded.**
* 30533: this node has only before_plan information. **Should be discarded.**
* 31089: has only 10 days of first_plan data. **Should be discarded.**
* 31997: this node has only before_plan information. **Should be discarded.**
* 34403: this node has only before_plan information. **Should be discarded.**
* 63353: this node has only before_plan information. **Should be discarded.**
* 64491: this node has only before_plan information. **Should be discarded.**
* 64655: this node has only before_plan information. **Should be discarded.**
* 65407: this node has only before_plan information. **Should be discarded.**
* 65427: this node has only before_plan information. **Should be discarded.**
* 65709: this node has only before_plan information. **Should be discarded.**
* 67191: this node has only before_plan information. **Should be discarded.**
* 67717: this node has only before_plan information. **Should be discarded.**
* 67813: this node has only before_plan information. **Should be discarded.**
* 68481: this node has only before_plan information. **Should be discarded.**
* 68599: this node has only before_plan information. **Should be discarded.**
'''

treat_list = [int(k) for k,v in target_pal.items() if v == -1]
target_pal['65529'] = 1.5625
target_pal['10409'] = -2
target_pal['8985'] = -2

# Setting up column for initial target pal with the dictionary
attributes['initial_target_pal'] = attributes.from_dict(target_pal, orient='index')


# Initial goal achieved built
attributes['initial_goal_achieved'] = attributes['before_plan_mean']/attributes['initial_target_pal']

print 'Saving ./intermediate_data/attributes.csv...'
############################################################
# Saving the csv file with the new attributes
############################################################
attributes.to_csv("./intermediate_data/attributes.csv")



