import pandas as pd
import pickle
import datetime
import numpy as np
import xml.etree.ElementTree as ET
import networkx as nx

activities_csv = "./intermediate_data/activities.csv"
attributes_csv = "./intermediate_data/attributes.csv"
gexf_file = "./input_data/dynamic_network.gexf"

out_activities = "./output_data/activities.csv"
out_attributes = "./output_data/attributes.csv"
out_gexf = "./output_data/dynamic_network.gexf"


# Read activities
activities = pd.read_csv(activities_csv)
activities['date'] = pd.to_datetime(activities['date'])
del activities['Unnamed: 0']

# Read attributes
attributes = pd.read_csv(attributes_csv)
attributes['start_plan_date'] = pd.to_datetime(attributes['start_plan_date'])
del attributes['Unnamed: 0']

## List of nodes to be discarded
# **Reason:** only before plan mean information and no data afterwards
discard_list = list(attributes[attributes['initial_target_pal']==-1]['id'])
print len(discard_list), ' nodes to be discarded.'

### Cleaning up the activities file
# Cleaning up the activities file
print '\nCleaning activities file...'

newcsv = open(out_activities, 'w')
activitiesCSV = open(activities_csv, 'r')

for line in activitiesCSV:
    node = line.split(',')[1]
    if node == 'id':
        newcsv.write(line)
        continue
    else:
        if int(node) not in discard_list:
            newcsv.write(line)

print '\n Saving new csv file in ' + str(out_activities)
newcsv.close()

### Cleaning up the attributes file
print '\nCleaning attributes file...'

for node in discard_list:
    attributes =  attributes[attributes['id'] != node]

print 'Saving attributes in ' + out_attributes
attributes.to_csv(out_attributes)

### Cleaning up the gexf file

tree = ET.ElementTree()
tree.parse(gexf_file)
root = tree.getroot()

graph = root.find('{"http://www.gexf.net/1.2draft}graph')
nodes = graph.find('{"http://www.gexf.net/1.2draft}nodes')
nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')

edges = graph.find('{"http://www.gexf.net/1.2draft}edges')
edgeList = edges.findall('{"http://www.gexf.net/1.2draft}edge')

print 'Initial nodes: #' + str(len(nodeList))
print '\nOriginal amount of edges (including duplicated): ', len(edgeList)

# Build a graph (without temporal dimension)
g = nx.Graph()

for node in nodes:
    node_id = node.attrib['id']
    if node_id != "":
        g.add_node(node_id)

print 'Nodes included: #', len(g.nodes())

repeated_edges = []
for edge in edges:
    edge_id = edge.get('id')
    edge_source = edge.get('source')
    edge_target = edge.get('target')
    if g.has_edge(edge_target, edge_source):
        repeated_edges.append(edge_id)
    g.add_edge(edge_source, edge_target)

print 'Edges included: #', len(g.edges())
print 'Repeated edges: #', len(repeated_edges)

## Remove nodes on the isolated_nodes list
print '\na) Remove nodes at the list of discard nodes from gexf file...'

for node in nodeList:
    node_id = node.attrib['id']
    if int(node_id) in discard_list:
        nodes.remove(node)
        continue

nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')
print 'Nodes remaining: #' + str(len(nodeList))

print '\nb) Remove edges with removed nodes as source or target...'

edgesWithNodesRemoved = []
for edge in edgeList:
    source = edge.get('source')
    target = edge.get('target')
    if int(source) in discard_list or int(target) in discard_list:
        edgesWithNodesRemoved.append(edge.attrib['id'])
        edges.remove(edge)

print '\nEdges removed: ', len(edgesWithNodesRemoved)

edgeList = edges.findall('{"http://www.gexf.net/1.2draft}edge')
print '\nRemaining edges (including duplicated): ',len(edgeList)

print '\nSaving new gefx file in ' + out_gexf
tree.write(out_gexf)