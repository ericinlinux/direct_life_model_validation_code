'''
The aim of this code is to create dicitonaries and pandas DataFrame containing the information of the nodes
'''

import pandas as pd
import xmltodict
import pickle
import matplotlib.pyplot as plt

#
gexf = "./input_data/dynamic_network.gexf"

# Converting to Gexf to Dict
with open(gexf) as fd:
    root = xmltodict.parse(fd.read())

graph = root['ns0:gexf']['ns0:graph']
nodes = graph['ns0:nodes']

print 'Number of nodes in the graph: ', len(nodes['ns0:node'])

listofNodes = nodes['ns0:node']

# Collecting attributes from the graph and generating a new dictionary with that
dictNodesStartPlan = dict()
for node in listofNodes:
    start_plan_date = ""
    dropout = ""
    gender = ""
    corp = ""
    country = ""
    bmi = ""
    nodeid = node['@id']
    #print nodeid
    listofAttributes = node['ns0:attvalues']['ns0:attvalue']
    for attrib in listofAttributes:
        (at,value) = attrib.values()
        if int(at) == 1:
            start_plan_date = value
        elif int(at) == 2:
            dropout = value
        elif int(at) == 3:
            gender = value
        elif int(at) == 4:
            corp = value
        elif int(at) == 5:
            country = value
        elif int(at) == 6:
            bmi = value
    d = {'start_plan_date': start_plan_date, 'dropout':dropout, 'gender':gender,
         'corp':corp, 'country':country, 'bmi':bmi}
    dictNodesStartPlan[nodeid] = d

# Convert to DataFrame
df = pd.DataFrame(dictNodesStartPlan)
df = df.T
df['id'] = df.index

initial_date = pd.to_datetime('2010-04-28')
df['initial_date'] = initial_date

df['dropout'] = pd.to_datetime(df['dropout'])

df['dropout (days)'] = df['dropout'] - df['initial_date']
df.loc[pd.isnull(df['dropout (days)']),'dropout (days)'] = pd.to_timedelta('0 days')
df['dropout (days)'] = df['dropout (days)'].astype('timedelta64[D]').astype(int)

del df['initial_date']


# Graphic
rows_dropout = df[~pd.isnull(df['dropout'])]
days_drop = rows_dropout['dropout (days)']
bins = range(max(days_drop)+1)
plt.figure(figsize=(20,12))
plt.title('Drop out days distribution', fontsize=28)
plt.xlabel('Day', fontsize=20)
plt.ylabel('Quantity (nodes)', fontsize=20)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
a = plt.hist(days_drop.values, bins=bins)
plt.savefig('./imgs/dropout_histogram.png')



df.to_pickle('./intermediate_data/attributes.pickle')
