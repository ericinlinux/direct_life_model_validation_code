import numpy as np
import networkx as nx
import pandas as pd
import pickle
import scipy.stats as stats
import datetime
import matplotlib.pyplot as plt

def calculate_errors(graphs_file, empiricalActivities):

    print '\n###################################################'
    print 'Reading the graphs...'

    graphs = pickle.load(open(graphs_file,'rb'))
    #print '# of graphs: ', len(graphs)


    # Creates a dictionary and collect data from the nodes. The last iteration will overcome the previous ones.
    SimulatedGoalsDict = {}
    for t in range(91):
        graph = graphs[t]
        for node in graph.nodes():
            if 'activityTimeLine' in graph.node[node]:
                SimulatedGoalsDict[node] = graph.node[node]['activityTimeLine']

    SimulatedGoalsDF = pd.DataFrame(SimulatedGoalsDict)
    SimulatedGoalsDF.columns.name = 'id'
    SimulatedGoalsDF.index.name = 'date (days)'

    print 'Dimensions of the simulation table: ', SimulatedGoalsDF.shape

    # Taking out nodes not present in one of the tables
    # getting list of nodes in each table
    nodesSimulated =  list(set(list(SimulatedGoalsDF.columns)))
    nodesEmpirical =  list(set(list(empiricalActivities.columns)))

    cols = list(SimulatedGoalsDF.columns)
    cols = map(int,cols)

    selectedEmpirical = empiricalActivities.ix[:,empiricalActivities.columns.isin(cols)]
    print 'New number of nodes on empirical table: ', len(selectedEmpirical.columns)

    # Nodes that were not included
    nodesSelectedSimulated =  list(set(list(selectedEmpirical.columns)))
    print 'Nodes not matching: ', set(cols) - set(nodesSelectedSimulated)

    # Drop the 2979 column
    SimulatedGoalsDF.drop('2979', axis=1, inplace=True)

    SimulatedGoalsDF.columns = SimulatedGoalsDF.columns.astype(int)

    '''Flattening the dataframes for statistics'''
    simulated_data = SimulatedGoalsDF.reindex_axis(sorted(SimulatedGoalsDF.columns), axis=1)
    empirical_data = selectedEmpirical.reindex_axis(sorted(selectedEmpirical.columns), axis=1)

    simulated_flat = np.array(simulated_data)
    simulated_flat = simulated_flat.flatten()

    empirical_flat = np.array(empirical_data)
    empirical_flat = empirical_flat.flatten()

    df = pd.DataFrame(simulated_flat, columns=['Simulated'])
    df['Empirical'] = empirical_flat
    print '\nOriginal size of dataset after flattening the data: ', df.shape
    df = df.dropna()
    print '\nRemaining data set after cleaning up all the missing data: ', df.shape

    df['Diff'] = df['Simulated'] - df['Empirical']
    df['Diff_Squared'] = df['Diff'] * df['Diff']
    df['Abs'] = np.abs(df['Diff'])
    df = df.reset_index(drop=True)

    print '\n---------------\nStatistics for file ', graphs_file
    print 'Mean (SSR): ', df['Diff_Squared'].mean()
    print 'Standard deviation (SSR): ', df['Diff_Squared'].std()
    print 'Variation (SSR): ', df['Diff_Squared'].var()
    print 'Median (SSR): ', df['Diff_Squared'].median()
    print '\n-----------------------'
    print 'Mean (Abs): ', df['Abs'].mean()
    print 'Standard deviation (Abs): ', df['Abs'].std()
    print 'Variation (Abs): ', df['Abs'].var()
    print 'Median (Abs): ', df['Abs'].median()


if __name__ == '__main__':
    activities = pd.read_csv('./clean_data/activities.csv')
    activities['date'] = activities['date'].astype(np.datetime64)
    del activities['Unnamed: 0']

    experiment_activities = activities[(activities['date (days)'] >= 0) & (activities['date (days)'] < 91)]
    # Pivot the table
    empiricalActivities = experiment_activities.pivot(index='date (days)', columns='id', values='goal_achieved')
    print 'Dimensions of the pivot table with the activities within 91 days: ', empiricalActivities.shape

    # List of nodes in the entire data set
    listNodes = list(set(activities['id']))
    print 'total nodes in the csv file: ', len(listNodes)


    graphs_model = './clean_data/graphs_pure_model.pickle'
    graphs_linear_increasing = './clean_data/graphs_linear_increasing.pickle'
    graphs_merged = './clean_data/graphs_merged.pickle'

    calculate_errors(graphs_model, empiricalActivities)
    calculate_errors(graphs_linear_increasing, empiricalActivities)
    calculate_errors(graphs_merged, empiricalActivities)
