import pandas as pd
import pickle
import matplotlib.pyplot as plt
import numpy as np

def plot_graphs(kind='pure_model'):
    # Read the graphs
    graphFile = './clean_data/graphs_' + kind + '.pickle'
    graphs = pickle.load(open(graphFile,'rb'))

    SimulatedGoalsDict = {}
    for t in range(91):
        graph = graphs[t]
        for node in graph.nodes():
            if 'activityTimeLine' in graph.node[node]:
                SimulatedGoalsDict[node] = graph.node[node]['activityTimeLine']


    SimulatedGoalsDF = pd.DataFrame(SimulatedGoalsDict)

    plt.style.use('ggplot')
    SimulatedGoalsDF.plot(title ="Goal Achieved (" + kind + ") per node",figsize=(15,10),legend=False, fontsize=12)
    plt.xlabel("Days",fontsize=12)
    plt.ylabel("Goal Achieved ",fontsize=12)
    plt.ylim(0.4, 1.7)
    figFile = './imgs/simulation_'+kind+'.png'
    plt.savefig(figFile)
    plt.show()

    csvFile = './clean_data/simulation_' + kind + '.csv'
    SimulatedGoalsDF.to_csv(csvFile)


def calculateError(kind ='pure_model'):
    activities = pd.read_csv('./clean_data/activities.csv')
    activities['date'] = activities['date'].astype(np.datetime64)
    del activities['Unnamed: 0']
    # Get the activities that happenned during the experimental time
    experiment_activities = activities[(activities['date (days)'] >= 0) & (activities['date (days)'] < 91)]

    empiricalActivities = experiment_activities.pivot(index='date (days)', columns='id', values='goal_achieved')

    SimulatedGoalsDF = pd.read_csv('./clean_data/simulation_' + kind + '.csv')
    SimulatedGoalsDF.columns.name = 'id'
    SimulatedGoalsDF.index.name = 'date (days)'
    del SimulatedGoalsDF['Unnamed: 0']

    # List of nodes in the entire data set
    listNodes = list(set(activities['id']))
    len(listNodes)

    nodesSimulated = list(set(list(SimulatedGoalsDF.columns)))
    nodesEmpirical = list(set(list(empiricalActivities.columns)))

    cols = list(SimulatedGoalsDF.columns)
    cols = map(int, cols)

    # Selection of empirical data
    selectedEmpirical = empiricalActivities.ix[:, empiricalActivities.columns.isin(cols)]
    nodesSelectedSimulated = list(set(list(selectedEmpirical.columns)))

    # Drop the 2979 column
    SimulatedGoalsDF.drop('2979', axis=1, inplace=True)

    nodesSimulated = list(set(list(SimulatedGoalsDF.columns)))
    SimulatedGoalsDF.columns = SimulatedGoalsDF.columns.astype(int)

    diffSimEmp = SimulatedGoalsDF - selectedEmpirical
    arrayDiff = np.array(diffSimEmp)

    #arrayDiffSquared = np.power(arrayDiff, 2)
    arrayDiffSquared = np.absolute(arrayDiff)

    print 'Mean error: \t', np.nanmean(arrayDiffSquared)
    print 'Std deviation: \t', np.nanstd(arrayDiffSquared)
    print 'Median: \t', np.nanmedian(arrayDiffSquared)
    print 'Variance: \t', np.nanvar(arrayDiffSquared)

if __name__ == '__main__':
    plot_graphs()
    calculateError()

    plot_graphs('linear_increasing')
    calculateError('linear_increasing')

    plot_graphs('merged')
    calculateError('merged')





