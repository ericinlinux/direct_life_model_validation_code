'''
Last checking on the gexf file and generating the clean data
'''

import xml.etree.ElementTree as ET
import pickle

folder = './intermediate_data/'
gexf = folder + "3_graphWithoutNodes.xml"

tree = ET.ElementTree()
tree.parse(gexf)
root = tree.getroot()

graph = root.find('{"http://www.gexf.net/1.2draft}graph')
nodes = graph.find('{"http://www.gexf.net/1.2draft}nodes')
nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')

edges = graph.find('{"http://www.gexf.net/1.2draft}edges')
edgeList = edges.findall('{"http://www.gexf.net/1.2draft}edge')

# Create list of nodes
listnodesGexf = []
for node in nodeList:
    listnodesGexf.append(node.attrib['id'])

print 'Number of nodes at gexf file: ', len(listnodesGexf)
listnodesGexf = map(int, listnodesGexf)

print 'Checking and removing edges with inconsistency...'
removedEdges = []
for edge in edgeList:
    source = edge.get('source')
    target = edge.get('target')
    if int(source) not in listnodesGexf or int(target) not in listnodesGexf:
        edges.remove(edge)
        removedEdges.append(edge.attrib['id'])

print 'Removed edges: ',len(removedEdges)

edgeList = edges.findall('{"http://www.gexf.net/1.2draft}edge')

print 'Remanining edges: ',len(edgeList)

# Saving the gexf file
tree.write('./intermediate_data/4_dynamic_network.gexf')