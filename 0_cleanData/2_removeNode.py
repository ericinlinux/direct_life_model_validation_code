''' After removing nodes from the gexf file, it is time to check if the CSV file contains nodes that are not at
the gexf file

'''
import xml.etree.ElementTree as ET
import pandas as pd
import pickle

gexf = "./intermediate_data/1_graphWithoutNodes.xml"
datacsv = "./intermediate_data/new_activities_per_day_phase_1.csv"

tree = ET.ElementTree()
tree.parse(gexf)
root = tree.getroot()

graph = root.find('{"http://www.gexf.net/1.2draft}graph')
nodes = graph.find('{"http://www.gexf.net/1.2draft}nodes')
nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')

print 'Starting cleaning of nodes from csv...'

print '\nInitial nodes: ' + str(len(nodeList))

activities = pd.read_csv(datacsv)
listOfNodesCSV = list(set(activities['id']))

print '\nNumber of nodes at CSV file: ', len(listOfNodesCSV)

removedNodes = []
for node in nodeList:
    n_id = node.attrib['id']
    if int(n_id) not in listOfNodesCSV:
        removedNodes.append(node.attrib['id'])
        nodes.remove(node)
        continue

print '\nNodes removed: ', len(removedNodes)
removedNodes = map(int, removedNodes)

print '\nSaving the list of nodes removed at ./intermediate_data/2_removedNodes.p...'
pickle.dump(removedNodes, open('./intermediate_data/2_removedNodes.p', 'w'))

print '\nSaving new gefx file without the nodes in ./intermediate_data/2_graphWithoutNodes.xml...'
tree.write('./intermediate_data/2_graphWithoutNodes.xml')