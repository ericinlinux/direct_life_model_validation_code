import xml.etree.ElementTree as ET
import datetime
import pickle
import networkx as nx
import pandas as pd

gexf = "./intermediate_data/4_dynamic_network.gexf"
activities = "./intermediate_data/new_activities_per_day_phase_1.csv"

tree = ET.ElementTree()
tree.parse(gexf)
root = tree.getroot()

graph = root.find('{"http://www.gexf.net/1.2draft}graph')
nodes = graph.find('{"http://www.gexf.net/1.2draft}nodes')
nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')

edges = graph.find('{"http://www.gexf.net/1.2draft}edges')
edgeList = edges.findall('{"http://www.gexf.net/1.2draft}edge')

print 'Initial nodes: #' + str(len(nodeList))
print '\nOriginal amount of edges (including duplicated): ', len(edgeList)


# Build a graph (without temporal dimension)
g = nx.Graph()

for node in nodes:
    node_id = node.attrib['id']
    if node_id != "":
        g.add_node(node_id)

print 'Nodes included: #', len(g.nodes())

repeated_edges = []
for edge in edges:
    edge_id = edge.get('id')
    edge_source = edge.get('source')
    edge_target = edge.get('target')
    if g.has_edge(edge_target, edge_source):
        repeated_edges.append(edge_id)
    g.add_edge(edge_source, edge_target)

print 'Edges included: #', len(g.edges())
print 'Repeated edges: #', len(repeated_edges)

# Getting the isolated nodes
isolated_node = []
for node in g.nodes():
    neigh = g.neighbors(node)
    if len(neigh) == 0:
        isolated_node.append(node)

print 'Nodes isolated: ', len(isolated_node)

# Removing the nodes and generating the final files
csvOutput = './clean_data/activities.csv'
gexfOutput = './clean_data/dynamic_network.gexf'

tree = ET.ElementTree()
tree.parse(gexf)
root = tree.getroot()

graph = root.find('{"http://www.gexf.net/1.2draft}graph')

# Get nodes reference
nodes = graph.find('{"http://www.gexf.net/1.2draft}nodes')
nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')

print 'Starting cleaning of nodes...'
print 'Initial nodes: #' + str(len(nodeList))

## Remove nodes on the isolated_nodes list
print '\na) Remove nodes at the list of isolated nodes from gexf file...'

for node in nodeList:
    node_id = node.attrib['id']
    if node_id in isolated_node:
        nodes.remove(node)
        continue

nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')
print 'Nodes remaining: #' + str(len(nodeList))

print '\nb) Cleaning csv file and conflicts...'

newcsv = open(csvOutput, 'w')
activitiesCSV = open(activities, 'r')

for line in activitiesCSV:
    node = line.split(',')[0]
    if node == 'id':
        newcsv.write(line)
        continue
    else:
        if node not in isolated_node:
            newcsv.write(line)

print '\n Saving new csv file in ' + str(csvOutput)
newcsv.close()

print 'Saving the new gexf file ' + str(gexfOutput) + '...'
tree.write(gexfOutput)