''' The first step is to remove all nodes that are not supposed to be at our data set.

Reasons to remove nodes:
1 - No attributes at Gexf file
2 - Dropout date before 28/04/2010 (our initial data for the network)

'''
import xml.etree.ElementTree as ET
import datetime
import pickle

gexf = "./raw_data/communalitics2015_dynamical.gexf"


tree = ET.ElementTree()
tree.parse(gexf)
root = tree.getroot()

graph = root.find('{"http://www.gexf.net/1.2draft}graph')

# Get nodes reference
nodes = graph.find('{"http://www.gexf.net/1.2draft}nodes')
nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')

print 'Starting cleaning of nodes...'

print 'Initial nodes: #' + str(len(nodeList))


## Remove nodes without any attribute
print '\na) Remove nodes without any attribute...'

nodesWithoutAttr = []
for node in nodeList:
    attvalues = node.find('{"http://www.gexf.net/1.2draft}attvalues')
    if attvalues == None:
        nodesWithoutAttr.append(node.attrib['id'])
        nodes.remove(node)

print 'Nodes removed (a): #' + str(len(nodesWithoutAttr))
nodesWithoutAttr = map(int, nodesWithoutAttr)

# Refreshing the information
nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')

print 'Nodes remaining: #' + str(len(nodeList))

# Removing nodes without start_date attribute
print '\nb) Remove nodes without start date attribute...'

nodesWithoutStartDate = []
for node in nodeList:
    rootAttvalues = node.find('{"http://www.gexf.net/1.2draft}attvalues')
    lstAttvalues = rootAttvalues.findall('{"http://www.gexf.net/1.2draft}attvalue')
    for att in lstAttvalues:
        if att.get('for') == '1':
            if att.get('value') == "":
                nodesWithoutStartDate.append(node.attrib['id'])
                nodes.remove(node)
                continue

print 'Nodes removed (b): #' + str(len(nodesWithoutStartDate))

nodesWithoutStartDate = map(int, nodesWithoutStartDate)
# Refresh the list of nodes
nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')
print 'Nodes remaining: #' + str(len(nodeList))



print '\nc) Remove nodes with dropout date before 28/04/2010...'

nodesDropedBeforeStartDate = []
start_date = datetime.datetime(2010,4,28)

for node in nodeList:
    rootAttvalues = node.find('{"http://www.gexf.net/1.2draft}attvalues')
    lstAttvalues = rootAttvalues.findall('{"http://www.gexf.net/1.2draft}attvalue')
    for att in lstAttvalues:
        if att.get('for') == '2':
            if att.get('value') == "":
                continue
            else:
                dropoutDate = datetime.datetime.strptime(att.get('value'), '%Y-%m-%d')
                if dropoutDate < start_date:
                    nodesDropedBeforeStartDate.append(node.attrib['id'])
                    nodes.remove(node)
                    continue

print 'Nodes removed (c): #' + str(len(nodesDropedBeforeStartDate))

nodesDropedBeforeStartDate = map(int, nodesDropedBeforeStartDate)
# Refresh the list of nodes
nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')
print 'Nodes remaining: #' + str(len(nodeList))


print '\nd) Cleaning csv file and conflicts...'

jointNodes2Remove = nodesWithoutAttr + nodesWithoutStartDate + nodesDropedBeforeStartDate
print 'Nodes to be removed from csv file: ' + str(len(jointNodes2Remove))

newcsv = open("./intermediate_data/new_activities_per_day_phase_1.csv", 'w')

pals = "./raw_data/activity_per_day.csv"
csvFile = open(pals, 'r')

for line in csvFile:
    node = line.split(',')[0]
    if node == 'id':
        newcsv.write(line)
        continue
    else:
        if int(node) not in jointNodes2Remove:
            newcsv.write(line)

print '\n Saving new csv file in ./intermediate_data/new_activities_per_day_phase_1.csv...'
newcsv.close()

print '\n Saving discarded nodes in ./intermediate_data/1_removedNodes.p...'
pickle.dump(jointNodes2Remove, open('./intermediate_data/1_removedNodes.p', 'w'))

print '\n Saving new gefx file without the nodes in ./intermediate_data/1_graphWithoutNodes.xml...'
tree.write('./intermediate_data/1_graphWithoutNodes.xml')