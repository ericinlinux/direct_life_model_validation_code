'''
Removing the edges without attributes first (start date)
'''


import xml.etree.ElementTree as ET
import pickle

gexf = "./intermediate_data/2_graphWithoutNodes.xml"

removedNodes1 = pickle.load(open('./intermediate_data/1_removedNodes.p', 'r'))
removedNodes2 = pickle.load(open('./intermediate_data/2_removedNodes.p', 'r'))

removedNodes = removedNodes1 + removedNodes2

print 'Nodes removed in previous processes: ', len(removedNodes)

'''
removedNodes has the list of all nodes that shouldn't participate on the network anymore. Time to remove the edges!
(1) Edges without start date
(2) Edges containing nodes that are note part of the network
'''
tree = ET.ElementTree()
tree.parse(gexf)
root = tree.getroot()

graph = root.find('{"http://www.gexf.net/1.2draft}graph')
edges = graph.find('{"http://www.gexf.net/1.2draft}edges')
edgeList = edges.findall('{"http://www.gexf.net/1.2draft}edge')

print '\nOriginal amount of edges (including duplicated): ', len(edgeList)

print '\na) Remove edges without attributes...'
edgesWithoutAttr = []
for edge in edgeList:
    attvalues = edge.find('{"http://www.gexf.net/1.2draft}attvalues')
    if attvalues == None:
        edgesWithoutAttr.append(edge.attrib['id'])
        edges.remove(edge)

print 'Edges removed: ', len(edgesWithoutAttr)

# Refresh the edges
edgeList = edges.findall('{"http://www.gexf.net/1.2draft}edge')
print '\nRemaining edges (including duplicated): ',len(edgeList)

print '\nb) Remove edges with removed nodes as source or target...'

edgesWithNodesRemoved = []
for edge in edgeList:
    source = edge.get('source')
    target = edge.get('target')
    if int(source) in removedNodes or int(target) in removedNodes:
        edgesWithNodesRemoved.append(edge.attrib['id'])
        edges.remove(edge)


print '\nEdges removed: ', len(edgesWithNodesRemoved)

edgeList = edges.findall('{"http://www.gexf.net/1.2draft}edge')
print '\nRemaining edges (including duplicated): ',len(edgeList)

totalRemovedEdges = edgesWithoutAttr + edgesWithNodesRemoved
print '\nTotal edges removed: ', len(totalRemovedEdges)


print '\nSaving the list of edges removed at ./intermediate_data/3_removedEdges.p...'
pickle.dump(totalRemovedEdges, open('./intermediate_data/3_removedEdges.p', 'w'))

print '\nSaving new gefx file without the edges in ./intermediate_data/3_graphWithoutNodes.xml...'
tree.write('./intermediate_data/3_graphWithoutNodes.xml')