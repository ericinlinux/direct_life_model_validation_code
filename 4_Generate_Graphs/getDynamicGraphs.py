import pickle
from datetime import datetime
import gexf2nx as g2n

min_date = datetime.strptime("2010-04-28", "%Y-%m-%d")
max_date = datetime.strptime("2010-07-28", "%Y-%m-%d")

rangeOfDays = (max_date - min_date).days

list_graphs = []
for day in range(rangeOfDays):
    g = g2n.gexf2nx(day)
    list_graphs.append(g)

pickle.dump(list_graphs, open('./data_set_output/list_graphs.pickle', 'wb'))