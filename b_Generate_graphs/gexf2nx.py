# Function to convert gexf to nx
import xml.etree.ElementTree as ET
from datetime import datetime
import networkx as nx
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def gexf2nx(day, graph):
    # Range of time
    min_date = datetime.strptime("2010-04-28", "%Y-%m-%d")

    branch_of_nodes = graph.find('{"http://www.gexf.net/1.2draft}nodes')
    nodes = branch_of_nodes.findall('{"http://www.gexf.net/1.2draft}node')

    branch_of_edges = graph.find('{"http://www.gexf.net/1.2draft}edges')
    edges = branch_of_edges.findall('{"http://www.gexf.net/1.2draft}edge')

    print 'Parsing: Day ' + str(day)
    #print 'Number of nodes originally: #', len(nodes)
    #print 'Number of edges: #', str(len(edges))

    # Getting attributes for the nodes
    attributes = pd.read_csv('./clean_data/attributes.csv')
    attributes['id'] = attributes['id'].astype(int)
    # attributes['start_plan_date'] = pd.to_datetime(attributes['start_plan_date'])
    # attributes['dropout'] = pd.to_datetime(attributes['dropout'])
    # attributes['new_start_plan_date'] = pd.to_datetime(attributes['new_start_plan_date'])
    attributes.index = attributes['id']
    #del attributes['Unnamed: 0']

    '''
    activities = pd.read_csv('./data_set_input/activities.csv')
    activities['date'] = pd.to_datetime(activities['date'])
    activities['goal_achieved'] = activities['pal'] / activities['target_pal']
    activities.loc[activities['goal_achieved'] == np.inf, 'goal_achieved'] = np.nan
    '''

    g = nx.Graph()

    # Node is added according to the day and to start date and dropout date
    removedNodes = []
    for node in nodes:
        skipNodeFlag = False
        node_id = node.attrib['id']
        start_plan = attributes.loc[int(node_id), 'start_plan_date (days)']

        # Node born later than the current day
        if start_plan > day:
            skipNodeFlag = True

        # Node dropped out on the current day
        dropout_date = str(attributes.loc[int(node_id), 'dropout (days)'])

        if dropout_date <= day:
            skipNodeFlag = True
                # Gender
        gender = attributes.loc[int(node_id), 'gender']
        # Corporation
        corporation = attributes.loc[int(node_id), 'corp']
        # Country
        country = attributes.loc[int(node_id), 'country']
        # BMI
        bmi = attributes.loc[int(node_id), 'bmi']
        # Initial values
        try:
            initial_target_pal = attributes.loc[int(node_id), 'initial_target_pal']
        except:
            initial_target_pal = np.nan
            print node_id

        initial_goal = attributes.loc[int(node_id), 'initial_goal_achieved']

        before_plan_mean = attributes.loc[int(node_id), 'before_plan_mean']


        end_first_plan = start_plan + 84

        if skipNodeFlag:
            removedNodes.append(node_id)
        else:
            g.add_node(node_id, start_plan=start_plan, dropout_date=dropout_date, gender=gender,
                       corporation=corporation, country=country, bmi=bmi, initial_goal=initial_goal,
                       initial_target_pal=initial_target_pal, before_plan_mean=before_plan_mean, end_first_plan=end_first_plan)

    print 'Nodes included: #', len(g.nodes())

    # Insert edges in graph g
    removedEdges = []
    repeated_edges = []
    for edge in edges:
        edge_id = edge.get('id')
        edge_source = edge.get('source')
        edge_target = edge.get('target')
        edge_start = edge.get('start')
        if (edge_id == "") | (edge_source == "") | (edge_target == "") | (edge_start == ""):
            print edge_id
            continue
        if edge_source in removedNodes or edge_target in removedNodes:
            removedEdges.append(edge_id)
            continue
        date_edge = datetime.strptime(edge_start, "%Y-%m-%d")
        days = (date_edge - min_date).days
        if days > day:
            removedEdges.append(edge_id)
            continue
        if g.has_edge(edge_target, edge_source):
            repeated_edges.append(edge_id)
        g.add_edge(edge_source, edge_target)

    print 'Edges included: #', len(g.edges())
    #print 'Edges ignored: #', len(removedEdges)
    print 'Edges duplicated: #', len(repeated_edges)
    #print 'Total Edges: #', len(g.edges())+len(removedEdges)
    return g


#gexf2nx(0)