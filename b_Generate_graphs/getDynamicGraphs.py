import pickle
from datetime import datetime
import gexf2nx as g2n
import xml.etree.ElementTree as ET


min_date = datetime.strptime("2010-04-28", "%Y-%m-%d")
max_date = datetime.strptime("2010-07-28", "%Y-%m-%d")

rangeOfDays = (max_date - min_date).days

inputFile = "./clean_data/dynamic_network.gexf"

tree = ET.ElementTree()
tree.parse(inputFile)
root = tree.getroot()
graph = root.find('{"http://www.gexf.net/1.2draft}graph')

list_graphs = []
for day in range(rangeOfDays):

    g = g2n.gexf2nx(day, graph)
    list_graphs.append(g)

pickle.dump(list_graphs, open('./clean_data/list_graphs.pickle', 'wb'))