
import pandas as pd
import numpy as np
import datetime
import removeListOfNodes as rln

# Table of attributes
attributes = pd.read_pickle('./clean_data/attributesPandas.pickle')

attributes['start_plan_date'] = pd.to_datetime(attributes['start_plan_date'])
attributes['dropout'] = pd.to_datetime(attributes['dropout'])

# Important Dates
date_start = datetime.datetime(2010,4,28)
week_before = datetime.datetime(2010,4,21)
end_date = datetime.datetime(2010,8,6)

# Getting all the nodes from the attributes table
listOfNodes = list(attributes.index)
listOfNodes = map(int,listOfNodes)

# Check all the nodes with start_plan_date after the date_start
after_start = []
before_start = []
for node in listOfNodes:
    spdate = attributes.loc[str(node),'start_plan_date']
    if spdate >= date_start:
        after_start.append(node)
    else:
        before_start.append(node)


# Initiate csv file
activities = pd.read_csv('./clean_data/activities_first_plan.csv')
activities['date'] = pd.to_datetime(activities['date'])
activities['goal_achieved'] = activities['pal']/activities['target_pal']
activities.loc[ activities['goal_achieved']==np.inf, 'goal_achieved'] = np.nan
del activities['Unnamed: 0']

'''
Nodes Before Start
'''
initialPAL = dict()

nodesWithNoWeekBefore = []
for node in before_start:
    # Get the mean goal_achieved for the node for the previous week
    actWeekBeforeStartDate = activities[(activities['date'] >= week_before) & (activities['date']< date_start) &
                                        (activities['id'] == node)]['goal_achieved'].mean()
    if np.isnan(actWeekBeforeStartDate):
        nodesWithNoWeekBefore.append(node)
    else:
        initialPAL[node] = actWeekBeforeStartDate

'''
Nodes after start
'''
nodesWithNoWeekAfter = []
# List of tuples with node and new start_plan_date
nodes_new_start_date = []
for node in after_start:
    # Get the start_plan_date and the week after
    startPlanDate = attributes[attributes['id'] == str(node)]['start_plan_date']
    weekLaterDate = startPlanDate + datetime.timedelta(days=7)
    # Get the activities on that week for the node
    actWeekAfterStartDate = activities[(activities['date'] >= startPlanDate[0]) &
                                       (activities['date'] < weekLaterDate[0]) &
                                       (activities['id'] == int(node))]
    meanNodeGoalAchieved = actWeekAfterStartDate['goal_achieved'].mean()

    if np.isnan(meanNodeGoalAchieved):
        nodesWithNoWeekAfter.append(node)
    else:
        initialPAL[node] = meanNodeGoalAchieved
        nodes_new_start_date.append((node,weekLaterDate))

'''
Now I have two lists with nodes that didn't receive any starting PAL: nodes2sendAfter and nodesNaN
 They sum up 91 nodes that will be threated in a different way.
 The proposal is: find for this nodes any time range where they have at least 4 time points in a
 row with the goal_achieved information. Then set up their start_plan_date to the next day after that.
'''

nodesWithData = []

# Detecting dead nodes without any data for the PAL in any day
for node in nodesWithNoWeekAfter:
    if np.isnan(activities[(activities['id'] == node) & (activities['date'] >= date_start) &
                     (activities['status'] != 'before_plan')]['goal_achieved'].mean()):
        continue
    else:
        nodesWithData.append(node)
        nodesWithNoWeekAfter.remove(node)

for node in nodesWithNoWeekBefore:
    if np.isnan(activities[(activities['id'] == node) &
                     (activities['date'] >= date_start) &
                     (activities['status'] != 'before_plan')]['goal_achieved'].mean()):
        continue
    else:
        nodesWithData.append(node)
        nodesWithNoWeekBefore.remove(node)

nodes2remove = nodesWithNoWeekBefore + nodesWithNoWeekAfter
# This is one of the 15 nodes that are being treated separately
nodes2remove.append(17945)
nodesWithData.remove(17945)

rln.removeListOfNodes(nodes2remove, './data_set_input/dynamic_network.gexf', './data_set_input/activities.csv')

'''
 Readjusting the nodes with data
'''
newDates = {
    29849: datetime.datetime(2010,6,18),
    1235:  datetime.datetime(2010,3,15),
    139:   datetime.datetime(2010,4,30),
    14311: datetime.datetime(2010,5,17),
    15117: datetime.datetime(2010,4,30),
    19589: datetime.datetime(2010,5,7),
    22675: datetime.datetime(2010,5,14),
    30939: datetime.datetime(2010,5,12),
    54603: datetime.datetime(2010,6,24),
    60463: datetime.datetime(2010,6,23),
    34093: datetime.datetime(2010,5,31),
    6727:  datetime.datetime(2010,5,1),
    8943:  datetime.datetime(2010,4,29),
    9135:  datetime.datetime(2010,5,4)
}

problems = []
for node in nodesWithData:
    startPlanDate = newDates[node]
    weekLaterDate = startPlanDate + datetime.timedelta(days=7)
    # Get the activities on that week for the node
    actWeekAfterStartDate = activities[(activities['date'] >= startPlanDate) & (activities['date'] < weekLaterDate) &
                                       (activities['id'] == int(node))]
    meanNodeGoalAchieved = actWeekAfterStartDate['goal_achieved'].mean()

    if np.isnan(meanNodeGoalAchieved):
        problems.append(node)
    else:
        initialPAL[node] = meanNodeGoalAchieved
        nodes_new_start_date.append((node, weekLaterDate))

if len(problems) == 0:
    print 'Clean!'
else:
    print problems
    print 'Problems!'
    exit()

# Insert column for the new start plan date
attributes['new_start_plan_date'] = np.nan

# New start dates for nodes altered
for (node, date) in nodes_new_start_date:
    attributes.loc[(attributes['id']==str(node)),'new_start_plan_date'] = date

# Including a column for the initial PAL to all nodes
attributes['id'] = attributes['id'].astype(float)
attributes['initial_pal'] = attributes['id'].map(initialPAL)

attributes.to_csv('./data_set_output/attributes.csv')

