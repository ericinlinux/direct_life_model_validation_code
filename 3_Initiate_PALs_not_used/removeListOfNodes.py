'''
Remove List of Nodes from the data set and generates a new data set
'''

import xml.etree.ElementTree as ET
import datetime
import pickle



def removeListOfNodes(nodes2remove, gexfFile, csvFile):
    # Output files
    csvOutput = './data_set_output/activities.csv'
    gexfOutput = './data_set_output/dynamic_network.gexf'

    tree = ET.ElementTree()
    tree.parse(gexfFile)
    root = tree.getroot()

    graph = root.find('{"http://www.gexf.net/1.2draft}graph')

    # Get nodes reference
    nodes = graph.find('{"http://www.gexf.net/1.2draft}nodes')
    nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')

    print 'Starting cleaning of nodes...'

    print 'Initial nodes: #' + str(len(nodeList))

    ## Remove nodes without any attribute
    print '\na) Remove nodes from gexf file...'

    for node in nodeList:
        node_id = node.attrib['id']
        if int(node_id) in nodes2remove:
            #print node
            nodes.remove(node)
            continue

    nodeList = nodes.findall('{"http://www.gexf.net/1.2draft}node')
    print 'Nodes remaining: #' + str(len(nodeList))

    print '\nb) Cleaning csv file and conflicts...'

    newcsv = open(csvOutput, 'w')

    activitiesCSV = open(csvFile, 'r')

    for line in activitiesCSV:
        node = line.split(',')[0]
        if node == 'id':
            newcsv.write(line)
            continue
        else:
            if int(node) not in nodes2remove:
                newcsv.write(line)

    print '\n Saving new csv file in ' + str(csvOutput)
    newcsv.close()

    edges = graph.find('{"http://www.gexf.net/1.2draft}edges')
    edgeList = edges.findall('{"http://www.gexf.net/1.2draft}edge')
    print '\nc) Remove edges with removed nodes as source or target...'
    print '\nOriginal amount of edges (including duplicated): ', len(edgeList)

    edgesWithNodesRemoved = []

    for edge in edgeList:
        source = edge.get('source')
        target = edge.get('target')
        if int(source) in nodes2remove or int(target) in nodes2remove:
            edgesWithNodesRemoved.append(edge.attrib['id'])
            edges.remove(edge)

    print '\nEdges removed: ', len(edgesWithNodesRemoved)

    edgeList = edges.findall('{"http://www.gexf.net/1.2draft}edge')
    print '\nRemaining edges (including duplicated): ', len(edgeList)

    print 'Saving the new gexf file ' + str(gexfOutput) + '...'
    tree.write(gexfOutput)

    print 'Nodes ', nodes2remove, ' removed with success!'

    return gexfOutput