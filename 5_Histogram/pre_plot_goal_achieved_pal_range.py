# Create a new column with the percentage of achievement to the target PAL for all the nodes, clean the
# inf rows and plot a histogram with the distribution of the percentages for all nodes between 28/4/2010 and 6/8/2010.

import pandas as pd
import numpy as np
import matplotlib.pylab as plt
import datetime


from matplotlib import style
style.use('ggplot')

#list_of_graphs = pickle.load(open('list_graphs.pickle','rb'))
activities = pd.read_csv('./input_data/activities.csv')

activities['date'] = pd.to_datetime(activities['date'])

activities['goal_achieved'] = activities['pal']/activities['target_pal']


initial_date = datetime.datetime(2010,4,28)
final_date = datetime.datetime(2010,7,28)

# Taking out the inf values
activities.loc[ activities['goal_achieved']==np.inf, 'goal_achieved'] = np.nan


range_activities = activities[(activities['date'] >= initial_date) & (activities['date']<= final_date)]

stitle = 'Histogram of achieved goals distribution \nbetween ' +  str(initial_date).split()[0] + ' and ' + str(final_date).split()[0]
plt.title(stitle, fontsize=20)

plt.xlabel('goal_achieved', fontsize=16)
plt.ylabel('Frequency', fontsize=14)
goals = range_activities[range_activities['goal_achieved']<2]['goal_achieved']

goals.hist(bins=200)

plt.savefig('./imgs/4_goal_achieved_histogram_range.png', bbox_inches='tight')
plt.show()