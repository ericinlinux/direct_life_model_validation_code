# Create a new column with the percentage of achievement to the target PAL for all the nodes, clean the
# inf rows and plot a histogram with the distribution of the percentages for all nodes.

import pandas as pd
import numpy as np
import matplotlib.pylab as plt

from matplotlib import style
style.use('ggplot')

#list_of_graphs = pickle.load(open('list_graphs.pickle','rb'))
activities = pd.read_csv('./input_data/activities.csv')

activities['goal_achieved'] = activities['pal']/activities['target_pal']

# Taking out the inf values
activities.loc[ activities['goal_achieved']==np.inf, 'goal_achieved'] = np.nan

plt.title('Histogram of achieved goals \ndistribution for the full data set', fontsize=20)

plt.xlabel('goal_achieved', fontsize=16)
plt.ylabel('Frequency', fontsize=14)
goals = activities[activities['goal_achieved']<2]['goal_achieved']

goals.hist(bins=200)

plt.savefig('./imgs/3_goal_achieved_histogram.png', bbox_inches='tight')
plt.show()